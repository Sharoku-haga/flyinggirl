﻿//==================================================================================================================================//
//!< @file		SceneFactory.cpp
//!< @brief		SceneFactoryクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "SceneFactory.h"
#include "../score/ScoreDataManager.h"
#include "logo/LogoScene.h"
#include "title/TitleScene.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{

/* Static Variable -------------------------------------------------------------------------------------------- */

score::ScoreDataManager*	SceneFactory::m_pScoreDataManager = nullptr;

/* Public Functions ------------------------------------------------------------------------------------------- */

void SceneFactory::InitializeMember(score::ScoreDataManager* pScoreDataManager)
{
	m_pScoreDataManager = pScoreDataManager;
}

Scene*	SceneFactory::CreateScene(Scene::ID id)
{
	Scene* pScene = nullptr;

	switch(id)
	{
	case Scene::LOGO:
		pScene = new logo::LogoScene();
		break;

	case Scene::TITLE:
		pScene = new title::TitleScene(m_pScoreDataManager);
		break;

	case Scene::GAME:
		break;

	case Scene::GAME_END:
		// テスト用処理. 本来ここには処理上こない. きたらError
		break;

	default: 
		// do nothing
		break;
	}

	return pScene;
}

}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
