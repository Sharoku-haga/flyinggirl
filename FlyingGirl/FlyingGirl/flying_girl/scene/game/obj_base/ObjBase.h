﻿//==================================================================================================================================//
//!< @file		ObjBase.h
//!< @brief		ObjBaseクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef OBJ_BASE_H
#define OBJ_BASE_H

#include <d3dx9.h>

class GameLib;

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace obj
{

//======================================================================//
//!< GameScene内のオブジェクトの基底クラス
//======================================================================//
class ObjBase
{

public:
	/** 
	* Constructor
	* @param[in] pos	位置座標
	* @param[in] texID	テクスチャーID
	*/
	ObjBase(D3DXVECTOR2 pos, int texID);

	/** Destructor */
	virtual ~ObjBase() = default;

	/** 
	* コントロール関数
	* @return  オブジェクトが消失したかどうか true = 消失している false = 消失していない 
	*/
	bool Control();

	/** 描画関数 */
	void Draw();

	/** 衝突判定時によばれる関数 */
	void CheckCollision();

	/** 
	* Getter
	* @return m_Pos		位置座標
	*/
	inline D3DXVECTOR2		GetPos(){ return m_Pos; }

protected:
	D3DXVECTOR2		m_Pos;					//!< 位置座標
	int				m_TexID;				//!< TextureID
	bool			m_HasVanished;			//!< 消滅したかどうか

	GameLib&		m_rGameLib;				//!< GameLibクラスのインスタンスへの参照

	/** 処理実行関数 */
	virtual void Run() = 0;

	/** 描画関数 */
	virtual void Render() = 0;

	/** 衝突判定処理を行う関数 */
	virtual void ExecuteCollisionProcessing() = 0;

};	// class ObjBase

}	// namespace obj		
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// OBJ_BASE_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
