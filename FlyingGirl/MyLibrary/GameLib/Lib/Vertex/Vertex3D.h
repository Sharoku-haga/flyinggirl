﻿//==================================================================================================================================//
//!< @file		Vertex3D.h
//!< @brief		Vertex3Dクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef VERTEX_3D_H
#define VERTEX_3D_H

#include "Vertex.h"
#include "CustomVertex.h"

/**
* 3D頂点情報を管理、描画するクラス
* 位置座標は左上
*/
class Vertex3D : public Vertex
{

public:

	/**
	* Constructor.
	* @param[in] pD3Device	デバイスへのポインタ
	* @param[in] width		幅
	* @param[in] height		高さ
	* @param[in] depth		奥行
	*/
	Vertex3D(LPDIRECT3DDEVICE9 pD3Device, float width, float height, float depth);

	/**Destructor*/
	virtual ~Vertex3D();

	/**
	* 描画関数.
	* @param[in] pTexture	テクスチャーへのポインタ
	* @param[in] posX		X軸の位置座標(デフォルト引数0.0f)
	* @param[in] posY		Y軸の位置座標(デフォルト引数0.0f)
	* @param[in] posZ		Z軸の位置座標(デフォルト引数0.0f)
	*/
	virtual void Draw(LPDIRECT3DTEXTURE9 pTexture, float posX = 0.0f, float posY = 0.0f, float posZ = 0.0f);

	/**
	* 頂点情報の先頭アドレスを取得する関数
	* @return 頂点情報
	*/
	inline virtual BASIC_VERTEX*	GetpVertex(){ return m_Vtx; }

private:
	CUSTOM_VERTEX_3D			m_Vtx[m_VtxNum];				//!< 頂点情報配列

	/**頂点情報体を作成する関数*/
	virtual void CreateVtx();

};


#endif	// VERTEX_3D_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
