﻿//==================================================================================================================================//
//!< @file		Vertex2D.h
//!< @brief		Vertex2Dクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef VERTEX_2D_H
#define VERTEX_2D_H

#include "Vertex.h"
#include "CustomVertex.h"

/**
* 2Dの頂点情報を管理、描画するクラス
* 位置座標が左上
*/
class Vertex2D : public Vertex
{

public:
	/**
	* Constructor.
	* @param[in] pD3Device	デバイスへのポインタ
	* @param[in] width		幅
	* @param[in] height		高さ
	*/
	Vertex2D(LPDIRECT3DDEVICE9 pD3Device, float width, float height);

	/**Destructor*/
	virtual ~Vertex2D();

	/**
	* 描画関数.
	* @param[in] pTexture	テクスチャーへのポインタ
	* @param[in] posX		X軸の位置座標(デフォルト引数0.0f)
	* @param[in] posY		Y軸の位置座標(デフォルト引数0.0f)
	* @param[in] posZ		Z軸の位置座標(デフォルト引数0.0f)
	*/
	virtual void Draw(LPDIRECT3DTEXTURE9 pTexture, float posX = 0.0f, float posY = 0.0f, float posZ = 0.0f);

	/**
	* 頂点情報の先頭アドレスを取得する関数
	* @return 頂点情報
	*/
	inline virtual BASIC_VERTEX*	GetpVertex(){ return m_Vtx; }

private:
	CUSTOM_VERTEX_2D			m_Vtx[m_VtxNum];				//!< 頂点情報配列

	/**頂点情報体を作成する関数*/
	virtual void CreateVtx();
			
};

#endif	// VERTEX_2D_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
