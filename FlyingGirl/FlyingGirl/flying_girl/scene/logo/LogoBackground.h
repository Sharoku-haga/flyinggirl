﻿//==================================================================================================================================//
//!< @file		LogoBackground.h
//!< @brief		LogoBackgroundクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef LOGO_BACKGROUND_H
#define LOGO_BACKGROUND_H

#include <d3dx9.h>

class GameLib;

namespace flying_girl
{
namespace scene
{
namespace logo
{

//======================================================================//
//!< ロゴシーンの背景クラス
//======================================================================//
class LogoBackground
{

public:
	/** 
	* Constructor
	* @param[in] texID	テクスチャーID
	*/
	explicit LogoBackground(int texID);

	/** Destructor */
	~LogoBackground();

	/**
	* コントロール関数
	* @return ロゴがフェードアウトしたかどうか
	*/
	bool Control();

	/** 描画関数 */
	void Draw();

private:
	/** LogoBackgroundの状態 */
	enum STATE
	{
		FADE_IN_LOGO,			//!< ロゴをフェードインしている状態
		DISPLAY_LOGO,			//!< ロゴを表示している状態
		FADE_OUT_LOGO,			//!< ロゴをフェードアウトしている状態
	};

	D3DXVECTOR2				m_Pos;					//!< 位置座標
	int						m_TexID;				//!< TextureのID
	int						m_VtxID;				//!< VertexのID
	int						m_DisplayTimeCount;		//!< ロゴ表示時間をカウントする変数
	STATE					m_State;				//!< 現在の状態を格納する変数
	BYTE					m_CurrentAlpha;			//!< 現在のアルファ値 
	bool					m_IsFadeOutLogo;		//!< ロゴがフェードアウトがしたかどうか

	GameLib&				m_rGameLib;				//!< GameLibクラス(Library)のインスタンスへの参照

};	// class LogoBackground 

}	// namespace logo
}	// namespace scene
}	// namespace flying_girl

#endif	// LOGO_BACKGROUND_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
