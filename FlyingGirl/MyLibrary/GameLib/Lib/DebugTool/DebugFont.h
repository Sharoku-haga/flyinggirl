﻿//==================================================================================================================================//
//!< @file		DebugFont.h
//!< @brief		DebugFontクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef DEBUG_FONT_H
#define DEBUG_FONT_H

#include<d3dx9.h>

/**
* デバック時に使用するフォントのクラス
*/
class DebugFont
{
public:
	/**
	* Constructor
	* @param[in] pD3Device	デバイスへのポインタ
	*/
	explicit DebugFont(LPDIRECT3DDEVICE9 pD3Device);

	/**Destructor*/
	~DebugFont();

	/**
	* テキストを描画する関数<br>
	* こちらは文字の色を変更できる
	* @param[in] pString	表示したい文字列
	* @param[in] rPos		テキストを表示したい座標
	*/
	void Draw(LPCTSTR pString, const D3DXVECTOR2& rPos);

	/**メモリ開放関数*/
	void Release();

	/**
	* 1文字の高さを取得する関数.
	* @return m_height	文字の高さ
	*/
	inline INT	 GetFontHeight(){ return m_Height; }

private:
	LPDIRECT3DDEVICE9	m_pD3Device;		//!< Direct3Dのデバイス
	LPD3DXFONT			m_pFont;			//!< ID3DXFontインターフェイスへのポインタ
	INT					m_Height;			//!< 文字の高さ
	UINT				m_Width;			//!< 文字の幅
	D3DCOLOR			m_Color;			//!< 文字の色

};

#endif // DEBUG_FONT_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
