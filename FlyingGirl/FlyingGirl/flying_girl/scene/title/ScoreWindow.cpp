﻿//==================================================================================================================================//
//!< @file		ScoreWindow.cpp
//!< @brief		ScoreWindowクラス実装
//!< @author	haga
//==================================================================================================================================//

/** @todo 2017/02/24現在 ウィンドウを表示するだけしか実装していない */

/* Includes --------------------------------------------------------------------------------------------------- */

#include "ScoreWindow.h"
#include "../../score/ScoreDataManager.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace title
{

/* Public Functions ------------------------------------------------------------------------------------------- */

ScoreWindow::ScoreWindow(score::ScoreDataManager* pScoreDataManager, int texID)
	: m_pScoreDataManager(pScoreDataManager)
	, m_Pos({0.0f, 0.0f})
	, m_TexID(texID)
	, m_IsDisplayEnd(false)
	, m_rGameLib(GameLib::Instance())
{
	// ウィンドウの大きさを取得し、それをもとにVetexを作成する
	float width = static_cast<float>(m_rGameLib.GetWindowWidth());
	float height = static_cast<float>(m_rGameLib.GetWindowHeight());
	m_VtxID = m_rGameLib.CreateVtx2D(width, height);
}

ScoreWindow::~ScoreWindow()
{
	m_rGameLib.ReleaseVtx(m_VtxID);
	// Textureは読み込んだところで解放する
}

bool ScoreWindow::Control()
{
	m_IsDisplayEnd = false;

	if(m_rGameLib.CheckKey(DIK_Z) == PUSH)
	{
		m_IsDisplayEnd = true;
	}
	else
	{
		// テスト用空処理. Zキーが押されていない場合
	}

	return m_IsDisplayEnd;
}

void ScoreWindow::Draw()
{
	m_rGameLib.Draw(m_TexID, m_VtxID, m_Pos.x, m_Pos.y);
}

}	// namespace title

}	// namespace scene

}	// namespace flying_girl

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
