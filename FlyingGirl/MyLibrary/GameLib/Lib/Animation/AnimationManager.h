﻿//==================================================================================================================================//
//!< @file		AnimationManager.h
//!< @brief		AnimationManagerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef ANIMATION_MANAGER_H
#define ANIMATION_MANAGER_H

#include <map>
#include "Animation.h"

/**
* Animationクラスを管理するクラス
*/
class AnimationManager
{

public:

	/**アニメ-ション再生モード*/
	enum MODE
	{
		ASCENDING_NUM,			//!< アニメーション番号をふやしていく
		DESCENDING_NUM,			//!< アニメーション番号をへらしていく
		ASCENDING_NUM_ROOP,		//!< ASCENDING_NUMをループする
		DESCENDING_NUM_ROOP,	//!< DESCENDING_NUMをループする
	};

	/**Constructor*/
	AnimationManager();

	/**Destructor*/
	~AnimationManager();

	/**
	* アニメーション情報を生成する関数
	* @param[in]	key			アニメ登録キー
	* @param[in]	tuCount		Tu方向アニメーションのカウント数
	* @param[in]	tuCount		Tv方向アニメーションのカウント数
	* @param[in]	interval	アニメーションの時間間隔	
	*/
	void CreateAnimation(int key, int tuCount, int tvCount, int interval);

	/**
	* アニメーションを更新する関数
	* @param[in]	key						アニメ登録キー
	* @param[in]	mode					アニメーション再生モード
	* @param[out]	pCurrentAnimeNum		現在のアニメーション番号
	* @param[out]	pIntervalCount			アニメーション時間カウント数
	* @return	アニメーションが終わったかどうか 終わっていたらtrue, 違うならfalse(ただしループの場合は常にfalse)
	*/
	bool Update(int key, MODE mode, int* pCurrentAnimeNum, int* pIntervalCount);

	/**
	* AnimationのUV値をセットする関数
	* @param[in]	key			アニメ登録キー
	* @param[in]	animeNum	アニメ番号
	* @param[in]	minTu		セットするTu最小値
	* @param[in]	maxTu		セットするTu最大値
	* @param[in]	minTv		セットするTv最小値
	* @param[in]	maxTv		セットするTv最大値
	*/
	void SetAnimation(int key,int animeNum, float minTu, float maxTu, float minTv, float maxTv);

	/**
	* アニメーションを開始する番号を取得する関数
	* @param[in]	key						アニメ登録キー
	* @param[in]	mode					アニメーション再生モード
	* @return アニメーションを開始する番号
	*/
	int GetStartAnimeNum(int key, MODE mode);

	/**
	* アニメーションのUV情報を取得する関数
	* @param[in]	key			Managerに登録しているキー
	* @param[in]	animeNum	アニメーション番号
	* @return	取得したいアニメーションのUV情報
	*/
	const Animation::AnimeUV&	GetAnimeUV(int key, int animeNum){ return m_pAnimation[key]->GetAnimeUV(animeNum); }

	/**
	* 指定したAnimationクラスを解放する関数
	* @param[in] key	アニメ登録キー
	*/
	void Release(int key);

	/**全てのAnimationクラスを解放する関数*/
	void ReleaseALL();

private:
	std::map<int, Animation*>	m_pAnimation;			//!< Animationクラスのインスタンスへのポインタを格納するmap

	/**
	* アニメーションをコントロールする関数
	* @param[in] key					アニメ登録キー
	* @param[out] pIntervalCount		アニメーション時間カウント数
	*/
	int Control(int key, int* pIntervalCount);

};

#endif	// ANIMATION_MANAGER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
