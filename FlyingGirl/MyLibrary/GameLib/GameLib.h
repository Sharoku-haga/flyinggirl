﻿//==================================================================================================================================//
//!< @file		GameLib.h
//!< @brief		GameLibクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef GAME_LIB_H
#define GAME_LIB_H

#include <windows.h>
#include <iostream>
#include <d3d9.h>
#include <d3dx9.h>

#define	DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

class WindowCreator;
class GraphicsDevice;
class InputDevice;
class InputKey;
class InputMouse;
class TextureManager;
class XFileManager;
class SoundFileManager;
class VertexManager;
class AnimationManager;
class DebugFont;
class DebugTimer;

#define DIRECT3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)		//!< 頂点フォーマット
#define USER_VERTEX_FVF (D3DFVF_XYZ |  D3DFVF_DIFFUSE | D3DFVF_TEX1)				//!< 頂点フォーマット(XYZとディフューズ色とテクスチャー)

/**アニメ-ション再生モード*/
enum class ANIME_MODE
{
	ASCENDING_NUM,			//!< アニメーション番号をふやしていく
	DESCENDING_NUM,			//!< アニメーション番号をへらしていく
	ASCENDING_NUM_ROOP,		//!< ASCENDING_NUMをループする
	DESCENDING_NUM_ROOP,	//!< DESCENDING_NUMをループする
};

/**DirectInputにおけるボタン、またはキーの状態*/
enum DI_STATE
{
	ON,				//!< 押した状態が続いている
	OFF,			//!< 離した状態が続いている
	PUSH,			//!< 押す
	RELEASE			//!< 離す
};

/**ホイールの状態*/
enum WHEEL_STATE
{
	ROLL_NONE,		//!< 回転していない
	ROLL_UP,		//!< 奥に回転させている状態
	ROLL_DOWN,		//!< 手前に回転させている状態
};

/**音楽の再生方法*/
enum SOUND_OPERATION
{
	SOUND_PLAY,			//!< 再生
	SOUND_STOP,			//!< 停止
	SOUND_LOOP,			//!< ループ再生
	SOUND_RESET, 		//!< 初期位置に戻す
	SOUND_RESET_PLAY,   //!< 停めて初期位置戻す
	SOUND_STOP_RESET	//!< 初期位置から再生し直す
};

/**Facadeパターンのライブラリのクラス*/
class GameLib
{

public:
	/**
	* GameLibの実体である関数<br>
	* Singletonパターン.
	* @return GameLibクラスのインスタンス
	*/
	static GameLib& Instance()
	{
		static GameLib gameLib;
		return gameLib;
	}

	//---------------------------------------------------------------------------------------------------------------------------------//
	// Init&Release functions
	//---------------------------------------------------------------------------------------------------------------------------------//

	/**
	* 初期化関数.
	* @param[in] title			ウィンドウタイトル
	* @param[in] width			ウィンドウの横幅
	* @param[in] height			ウインドウの縦幅
	* @param[in] Wndproc		ウィンドウプロシージャ関数
	* @param[in] isFullScreen	フルスクリーンかどうかのフラグ trueならフルスクリーン,falseなら通常
	* @param[in] hasIcon		アイコンを持つかどうかのフラグ
	* @param[in] iconID			アイコンID
	* @return	初期化が成功したかどうか S_OK = 成功, E_FAIL = 失敗
	*/
	HRESULT InitGameLib(TCHAR*  title, int width, int height, LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM),
		bool isFullScreen, bool hasIcon = false, WORD iconID = 0);

	/**メモリ開放関数*/
	void ReleaseGameLib();

	//---------------------------------------------------------------------------------------------------------------------------------//
	// Windoows functions
	//---------------------------------------------------------------------------------------------------------------------------------//

	/**ウィンドウをフルスクリーンかウィンドウサイズに変える関数*/
	void ChangeWindowMode();

	/**ウィンドウの横幅を取得する関数*/
	int GetWindowWidth();

	/**ウィンドウの縦幅を取得する関数*/
	int GetWindowHeight();

	//---------------------------------------------------------------------------------------------------------------------------------//
	// GraphicsDevice functions
	//---------------------------------------------------------------------------------------------------------------------------------//

	/**
	* デバイスを取得する関数.
	* @return デバイスのポインタ
	*/
	IDirect3DDevice9* GetDevice();

	/**
	* 描画開始処理関数.
	* @param[in] FVF 頂点フォーマット
	*/
	void StartRender(DWORD FVF = DIRECT3DFVF_CUSTOMVERTEX);

	/**描画終了処理関数*/
	void EndRender();

	/**
	* 頂点フォーマットを設定する関数
	* @param[in] FVF 頂点フォーマット
	*/
	void SetFVF(DWORD FVF);

	//---------------------------------------------------------------------------------------------------------------------------------//
	// Texture functions
	//---------------------------------------------------------------------------------------------------------------------------------//

	/**
	* テクスチャーを読み込む関数.
	* @param[in] pFilepath   ファイル名
	* @return	登録したID
	*/
	int LoadTex(TCHAR* pFilePath);

	/**
	* テクスチャーを詳細設定して読み込む関数.
	* @param[in] pFilepath		ファイルパス
	* @param[in] alpha			アルファ値
	* @param[in] red			色のR値 255
	* @param[in] green			色のG値 255
	* @param[in] blue			色のB値 255
	* @param[in] IsPowerTwo		テクスチャーのサイズが２のべき乗ならtrue,違うならfalse(デフォルト引数:false)
	*/
	int LoadTexEx(TCHAR* pFilePath, int alpha, int red, int green, int blue, bool IsPowerTwo = false);

	/**
	* テクスチャーのポインタを取得する関数.
	* @prama texkey テクスチャーを登録したキー、またはID
	* @return キーに一致したテクスチャーのポインタ
	*/
	LPDIRECT3DTEXTURE9 GetpTexture(int texKey);

	/**
	* 現在読み込んでいるテクスチャーを解放する関数
	* @param[in] texKey	登録したキー、またはID
	*/
	void ReleaseTex(int texKey);

	/**現在読み込んでいるテクスチャーの全て解放する関数*/
	void ReleaseTexALL();

	//-------------------------------------------------------------------------------------------------------------------------------//
	// Vertex functions
	//-------------------------------------------------------------------------------------------------------------------------------//

	/**
	* 頂点情報(2D)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @return 登録したキー(添え字)	
	*/
	int CreateVtx2D(float width, float height);

	/**
	* 位置座標が中心にある場合の頂点情報(2D)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @return 登録したキー(添え字)
	*/
	int CreateVtx2DCenterPos(float width, float height);

	/**
	* 頂点情報(3Dのxy平面ポリゴン)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth	奥行き
	* @return 登録したキー(添え字)
	*/
	int CreateVtx3D(float width, float height, float depth);

	/**
	* 位置座標が中心にある場合の頂点情報(3Dのxy平面ポリゴン)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth	奥行き
	* @return 登録したキー(添え字)
	*/
	int CreateVtx3DCeneterPos(float width, float height, float depth);

	/**
	* 頂点情報(3Dのxz平面ポリゴン)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth	奥行き
	* @return 登録したキー(添え字)	
	*/
	int CreateVtx3Dxz(float width, float height, float depth);

	/**
	* 位置座標が中心にある場合の頂点情報(3Dのxz平面ポリゴン)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth	奥行き
	* @return 登録したキー(添え字)
	*/
	int CreateVtx3DxzCeneterPos(float width, float height, float depth);

	/**
	* サイズを変更する関数
	* @param[in] key	登録したキー
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth  奥行(Z軸)
	*/
	void SetVtxSize(int key, float width, float height, float depth = 0.0f);

	/**
	* tu値とtv値を変更する関数
	* @param[in] key	登録したキー
	* @param[in] tuMin  tu値の最小値
	* @param[in] tuMax  tu値の最大値
	* @param[in] tvMin  tv値の最小値
	* @param[in] tvax	tv値の最大値
	*/
	void SetVtxTuTv(int key, float tuMin, float tuMax, float tvMin, float tvMax);

	/**
	* 色を変更する関数
	* @param[in] key	登録したキー
	* @param[in] color  色
	*/
	void SetVtxColor(int key, DWORD color);

	/**
	* UVスクロールを行う関数
	* @param[in] key			登録したキー
	* @param[in] scrollSpeedTu	tuのスクロールの速さ(変化量)
	* @param[in] scrollSpeedTv	tvのスクロールの速さ(変化量)
	*/
	void ScrollUV(int key, float scrollSpeedTu, float scrollSpeedTv);

	/**
	* 頂点情報の横幅を取得する関数
	* @param Key		検索キー
	* @return 頂点情報の横幅
	*/
	float GetVtxWidth(int Key);

	/**
	* 頂点情報の縦幅を取得する関数
	* @param Key	検索キー
	* @return 頂点情報の縦幅
	*/
	float GetVtxHeight(int Key);

	/**
	* 頂点情報の奥行を取得する関数
	* @param Key	検索キー
	* @return 頂点情報の奥行
	*/
	float GetVtxDepth(int Key);

	/**
	* 現在作成している頂点情報を解放する関数
	* @param[in] key	検索キー
	*/
	void ReleaseVtx(int key);

	/**現在作成している頂点情報全てを開放する関数*/
	void ReleaseVtxALL();

	//--------------------------------------------------------------------------------------------------------------------------------//
	// Animation functions
	//--------------------------------------------------------------------------------------------------------------------------------//

	/**
	* アニメーション情報を生成する関数
	* @param[in]	vtxKey		アニメーションを登録するキー
	* @param[in]	tuCount		Tu方向アニメーションのカウント数
	* @param[in]	tuCount		Tv方向アニメーションのカウント数
	* @param[in]	interval	アニメーションの時間間隔	
	*/
	void CreateAnimation(int key, int tuCount, int tvCount, int interval);

	/**
	* アニメのUV情報をセットする関数
	* @param[in]	key			アニメ登録キー
	* @param[in]	animeNum	アニメ-ションパターン番号
	* @param[in]	minTu		セットするTu最小値
	* @param[in]	maxTu		セットするTu最大値
	* @param[in]	minTv		セットするTv最小値
	* @param[in]	maxTv		セットするTv最大値
	*/
	void SetAnimeInfo(int key, int animeNum, float minTu, float maxTu, float minTv, float maxTv);

	/**
	* アニメーションパターン番号を更新する関数
	* @param[in]	key						アニメ登録キー
	* @param[in]	mode					アニメーション再生モード
	* @param[out]	pCurrentAnimeNum		現在のアニメ-ションパターン番号
	* @param[out]	pIntervalCount			アニメーション時間カウント数
	* @return	アニメーションが終わったかどうか 終わっていたらtrue, 違うならfalse(ただしループの場合は常にfalse)
	*/
	bool Update(int key, ANIME_MODE mode, int* pCurrentAnimeNum, int* pIntervalCount);

	/**
	* 登録しているアニメーション情報を解放する関数
	* @param[in] key	登録したキー、またはID
	*/
	void ReleaseAnimation(int key);

	/**登録しているアニメーション情報全てを解放する関数*/
	void ReleaseAnimationALL();

	//---------------------------------------------------------------------------------------------------------------------------------//
	// Drawing functions
	//---------------------------------------------------------------------------------------------------------------------------------//

	/**
	* 描画する関数<br>
	* @param[in] texKey	テクスチャーを登録したキー、またはID
	* @param[in] vtxKey 頂点情報を登録したキー、またはID
	* @param[in] posX	x座標(デフォルト引数は0.0f)
	* @param[in] posY   y座標(デフォルト引数は0.0f)
	* @param[in] posZ   z座標(デフォルト引数は0.0f)
	*/
	void Draw(int texKey, int vtxKey, float posX = 0.0f, float posY = 0.0f, float posZ = 0.0f);

	/**
	* アニメーションがあるテクスチャーを描画する関数.
	* @param[in] texKey		テクスチャーを登録したキー、またはID
	* @param[in] vtxKey		頂点情報を登録したキー、またはID
	* @param[in] animeKey	アニメーションキー
	* @param[in] animeNum	アニメ-ションパターン番号
	* @param[in] posX		x座標
	* @param[in] posY		y座標
	* @param[in] posZ		z座標(デフォルト引数は0.0f)
	*/
	void DrawAnime(int texKey, int vtxKey, int animeKey, int animeNum, float posX, float posY, float posZ = 0.0f);

	//----------------------------------------------------------------------------------------------------------------------------------//
	// Xfile functions
	//----------------------------------------------------------------------------------------------------------------------------------//
	
	/**
	* Xファイルを読み込む関数.
	* @param[in] pFilepath   ファイル名
	* @return		登録した番号(キー)を返す
	*/
	int LoadXFile(const TCHAR* pFilePath);

	/**
	* Xファイルを描画する関数.
	* @param[in] key	登録したキー、またはID
	*/
	void DrawXFile(int key);

	/**
	* 登録しているXファイルを解放する関数.
	* @param[in] key	登録したキー、またはID
	*/
	void ReleaseXFile(int key);

	/**登録しているXファイル全てを解放する関数*/
	void ReleaseXFileALL();

	//---------------------------------------------------------------------------------------------------------------------------------//
	// DirectInput functions
	//---------------------------------------------------------------------------------------------------------------------------------//

	/**DirectInputをアップデートする関数*/
	void UpdateDI();

	/**
	* キーの状態を確認する関数<br>
	* @param[in] DIK      ダイレクトインプットキー
	* @return ボタンの状態
	*/
	DI_STATE CheckKey(int DIK);

	/**
	* マウスの左ボタンの状態を取得する関数
	* @return ボタンの状態
	*/
	DI_STATE ChecKMouseL();

	/**
	* マウスの右ボタンの状態を取得する関数
	* @return ボタンの状態
	*/
	DI_STATE ChecKMouseR();

	/**
	* マウスのホイールの状態を取得する関数.
	* @return ホイールの状態
	*/
	WHEEL_STATE GetWheelState();

	/**
	* マウスの座標を取得する関数<br>
	* @param[out] mousePosX マウス座標xを格納する変数
	* @param[out] mousePosY マウス座標yを格納する変数
	*/
	void GetMousePos(float* mousePosX, float* mousePosY);

	/**
	* マウスカーソルの座標をセットする関数<br>
	* @param[out] posX マウスのx座標にセットするクライアント領域におけるx座標
	* @param[out] posY マウスのx座標にセットするクライアント領域におけるy座標
	*/
	void SetMouseCursorPos(float posX, float posY);
 
	/**
	* マウスカーソルを表示するかどうかを設定する関数
	* @param[in] isVisible 表示はtrue,非表示ならfalse
	* @attention この関数はフレームごとによぶとおかしい挙動になるので、注意
	*/
	void ShowMouseCursor(bool isVisible);

	//----------------------------------------------------------------------------------------------------------------------------------//
	// Sound functions
	//----------------------------------------------------------------------------------------------------------------------------------//
	
	/**
	* 音声を読み込む関数.
	* @param[in] pFilePath ファイル名
	*/
	int LoadSound(TCHAR* pFilePath);

	/**
	* 音楽を鳴らす関数
	* @param[in] key		 登録するキー、またはID(enumで作成を想定)
	* @param[in] operation   音楽の再生方法
	*/
	void PlayDSound(int key, SOUND_OPERATION operation);

	/**
	* 登録しているサウンドを解放する関数.
	* @param[in] key	登録したキー、またはID
	*/
	void ReleaseSound(int key);

	/**登録しているサウンド全てを解放する関数*/
	void ReleaseSoundALL();

	//----------------------------------------------------------------------------------------------------------------------------------//
	// Debug functions
	//----------------------------------------------------------------------------------------------------------------------------------//
	
	/**
	* デバック用の文字を表示させる関数.
	* @param[in] rText 表示した文字
	* @param[in] posX x座標
	* @param[in] posY y座標
	*/
	void DrawDebugFont(const std::string& rText, float posX, float posY);

	/**
	* デバック用の時間計測を開始する関数.
	* @param[in] rTimeName	計測したい時間の名前
	*/
	void StartTimer(const std::string& rTimeName);

	/**
	* デバック用の時間計測を終了する関数.
	* @param[in] rTimeName	計測を終えたい時間の名前
	*/
	void EndTimer(const std::string& rTimeName);

	/**
	* デバック用の時間計測した結果を取得する関数.
	* @param[in] rTimeName	取得したい計測時間の名前
	*/
	DWORD GetMeasuringTime(const std::string& rTimeName);

	/**
	* デバック用の計測結果を表示する関数.
	* @param[in] rTimeName	表示したい計測時間の名前
	* @param[in] posX x座標
	* @param[in] posY y座標
	*/
	void DrawMeasuringTime(const std::string& rTimeName, float posX, float posY);

	/**
	* デバック用の時間計測した結果全てとその合計時間を表示する関数.
	* @param[in] posX x座標
	* @param[in] posY y座標
	*/
	void DrawAllMeasuringTime(float posX, float posY);

	//----------------------------------------------------------------------------------------------------------------------------------//
	// Destructor
	//----------------------------------------------------------------------------------------------------------------------------------//

	/**Destructor*/
	~GameLib();

private:
	WindowCreator*			m_pWindowCreator;			//!< WindowCreatorクラスのインスタンスへのポインタ
	GraphicsDevice*			m_pGraphicsDevice;			//!< GraphicsDeviceクラスのインスタンスへのポインタ
	InputDevice*			m_pInputDevice;				//!< InputDeviceクラスのインスタンスへのポインタ
	InputKey*				m_pInputKey;				//!< InputKeyクラスのインスタンスへのポインタ
	InputMouse*				m_pInputMouse;				//!< InputMouseクラスのインスタンスへのポインタ
	TextureManager*			m_pTextureManager;			//!< TextureManagerクラスのインスタンスへのポインタ
	XFileManager*			m_pXFileManager;			//!< XFileManagerクラスのインスタンスへのポインタ
	SoundFileManager*		m_pSoundFileManager;		//!< SoundFileManagerクラスのインスタンスへのポインタ
	VertexManager*			m_pVertexManager;			//!< VertexManagerクラスのインスタンスへのポインタ
	AnimationManager*		m_pAnimationManager;		//!< AnimationManagerクラスのインスタンスへのポインタ
	DebugTimer*				m_pDebugTimer;				//!< DebugTimerクラスのインスタンスへのポインタ
	int						m_wWidth;					//!< ウインドウの幅
	int						m_wHeight;					//!< ウィンドウの高さ

	/**
	* Constructor.
	* Singltonパターンなのでprivate
	*/
	GameLib();

};

#endif	// GAME_LIB_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
