﻿//==================================================================================================================================//
//!< @file		TitleText.cpp
//!< @brief		TitleTextクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "TitleText.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace title
{

/* Unnamed Namespace ------------------------------------------------------------------------------------------ */

namespace
{

const BYTE		AlphaMaxVal		= 0xff;		// アルファ値の最大値
const BYTE		FadeSpeed		= 0x05;		// フェードのスピード(アルファ値)
const BYTE		ColorDefaultVal = 0xff;		// カラーの設定するときのデフォルト値

}

/* Public Functions ------------------------------------------------------------------------------------------- */

TitleText::TitleText(int texID)
	: m_Pos({0.0f, 0.0f})
	, m_TexID(texID)
	, m_CurrentAlpha(0x00)
	, m_IsFadeInEnd(false)
	, m_rGameLib(GameLib::Instance())
{
	// ウィンドウの大きさを取得し、それをもとにVetexを作成する
	float width		= static_cast<float>(m_rGameLib.GetWindowWidth());
	float height	= static_cast<float>(m_rGameLib.GetWindowHeight());
	m_VtxID			= m_rGameLib.CreateVtx2D(width, height);
}

TitleText::~TitleText()
{
	m_rGameLib.ReleaseVtx(m_VtxID);
	// Textureは読み込んだところで解放する
}

bool TitleText::Control()
{
	if(m_CurrentAlpha == AlphaMaxVal)
	{
		m_IsFadeInEnd = true;
	}
	else
	{
		m_CurrentAlpha += FadeSpeed;
	}

	return m_IsFadeInEnd;
}

void TitleText::Draw()
{
	m_rGameLib.SetVtxColor(m_VtxID, D3DCOLOR_ARGB(m_CurrentAlpha, ColorDefaultVal, ColorDefaultVal, ColorDefaultVal));
	m_rGameLib.Draw(m_TexID, m_VtxID, m_Pos.x, m_Pos.y);
}

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
