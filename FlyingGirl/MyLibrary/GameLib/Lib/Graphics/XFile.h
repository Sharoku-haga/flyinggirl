﻿//==================================================================================================================================//
//!< @file		XFile.h
//!< @brief		XFileクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef XFILE_H
#define XFILE_H

#include <d3dx9.h>

/**Xファイルを扱うクラス*/
class XFile
{

public:
	/**
	* Constructor
	* @param[in] pD3Device	デバイスへのポインタ
	*/
	explicit XFile(LPDIRECT3DDEVICE9 pD3Device);

	/**Destructor*/
	~XFile();

	/**
	* Xファイルからデータを読み込む関数.
	* @param[in] pFileName ファイル名
	* @retval	true	読み込み成功
	* @retval	false	読み込み失敗
	*/
	void LoadXFile(const TCHAR* pFileName);

	/**メッシュを描画する関数*/
	void Draw();

	/**メモリ解放関数*/
	void Release();

private:
	LPDIRECT3DDEVICE9		m_pD3Device;		//!< Direct3Dのデバイス
	LPD3DXMESH				m_pMesh;			//!< ID3DXMeshのインターフェイスへのポインタ
	D3DMATERIAL9*			m_pMaterials;		//!< マテリアル
	LPDIRECT3DTEXTURE9*		m_pTextures;		//!< テクスチャー
	DWORD					m_dwNumMaterials;	//!< マテリアル数

};

#endif	// XFILE_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
