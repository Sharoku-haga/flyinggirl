﻿//==================================================================================================================================//
//!< @file		ButtonFactory.cpp
//!< @brief		ButtonFactoryクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "ButtonFactory.h"
#include "btn.func/ScaleFunction.h"
#include "btn.func/BrightnessFunction.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace btn
{

/* Public Functions ------------------------------------------------------------------------------------------- */

Button*	ButtonFactory::AddScaleFunction(Button* pButton, float scaleVal)
{
	Button* pNewButton = nullptr;
	pNewButton = new func::ScaleFunction(pButton, scaleVal);
	return pNewButton;
}

Button*	ButtonFactory::AddBrightnessFunction(Button* pButton, DWORD brightnessVal)
{
	Button* pNewButton = nullptr;
	pNewButton = new func::BrightnessFunction(pButton, brightnessVal);
	return pNewButton;
}

}	// namespace btn
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
