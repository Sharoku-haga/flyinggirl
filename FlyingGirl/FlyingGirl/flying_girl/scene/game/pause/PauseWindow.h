﻿//==================================================================================================================================//
//!< @file		PauseWindow.h
//!< @brief		PauseWindowクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef PAUSE_WINDOW_H
#define PAUSE_WINDOW_H

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace pause
{

class PauseWindow
{

public:
	/** Constructor */
	PauseWindow();

	/** Destructor */
	~PauseWindow();

};	// class PauseWindow

}	// namespace pause 
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// PAUSE_WINDOW_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
