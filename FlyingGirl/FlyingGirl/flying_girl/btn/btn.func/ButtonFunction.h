﻿//==================================================================================================================================//
//!< @file		ButtonFunction.h
//!< @brief		ButtonFunctionクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef BUTTON_FUNCTION_H
#define BUTTON_FUNCTION_H

#include "../Button.h"

namespace flying_girl
{
namespace btn
{
namespace func
{

//======================================================================//
//!< ボタンの機能の基底クラス
//!< @attention BasicButtonクラスと一緒に必ず使用すること
//======================================================================//
class ButtonFunction : public Button
{

public:
	/**
	* Constructor
	* @param[in] pbutton	Buttonクラスのインスタンスへのポインタ
	*/
	explicit ButtonFunction(Button*	pButton);

	/**Desutructor*/
	virtual ~ButtonFunction();
	
protected:
	Button*			m_pButton;			//!< Buttonクラスのインスタンスへのポインタ

	/** 処理実行関数 */
	virtual void	Run()override = 0;

	/** 描画処理関数 */
	virtual void	Render()override = 0;

	/**
	* VetexIDを取得する関数
	* @return VertexID
	*/
	virtual int		GetVertexID()override{ return m_pButton->GetVtxID(); }

};	// class ButtonFunction

}	// namespace func
}	// namespace btn
}	// namespace flying_girl

#endif	// BUTTON_FUNCTION_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
