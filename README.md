# 少女大飛行 #
![README.png](https://bitbucket.org/repo/E5R8EG/images/2250590741-README.png)


[![](https://img.shields.io/badge/Visual%20Studio-2013%20Community-blue.svg)](https://www.microsoft.com/ja-jp/dev/products/community.aspx)
[![](https://img.shields.io/badge/DirectX%20SDK-9.0c%20June2010-yellow.svg)](https://www.microsoft.com/en-us/download/details.aspx?id=6812)

## リポジトリ概要 ##


自作弾幕シューティングゲーム「少女飛行」のリポジトリです。


2016年夏に制作していたシューティングゲームを一から作り直す企画です。


## ゲーム制作の目的 ##
以下の目的の為にこのゲームの作成を行います。




・自分のライブラリの機能向上


・VBAなどを使用した外部ツールをつくる


・オリジナル弾幕を作成する

## プロジェクトをビルドするための準備 ##
プロジェクトをビルドするには必要なツールとsdkを用意する必要があります。


ダウンロードは上記のバッジのリンクからお願いします。