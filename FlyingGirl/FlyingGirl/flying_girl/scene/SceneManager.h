﻿//==================================================================================================================================//
//!< @file		SceneManager.h
//!< @brief		SceneManagerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include "Scene.h"

namespace flying_girl
{

namespace score
{

class ScoreDataManager;

}	// namespace score

namespace scene
{

//======================================================================//
//!< Sceneクラスを管理するクラス
//======================================================================//
class SceneManager
{

public:

	/** Constructor */
	SceneManager();

	/** Destructor */
	~SceneManager();

	/**
	* Sceneを更新する関数
	* @return	ゲームが終了したかどうか.true →終了, false→継続
	*/
	bool	Update();

private:
	/** Scene遷移処理過程 */
	enum STEP
	{
		CREATE_SCENE,		//!< Scene生成
		UPDATE_SCENE,		//!< Scene更新
		DELETE_SCENE,		//!< Scene削除
	};

	Scene*							m_pScene;						//!< Sceneクラスのインスタンスへのポインタ
	score::ScoreDataManager*		m_pScoreDataManager;			//!< score::ScoreDataManagerクラスのインスタンスへのポインタ
	STEP							m_CurrentStep;					//!< 現在の処理の過程を格納する変数
	Scene::ID						m_CurrentSceneID;				//!< 現在のSceneIDを格納する変数
	Scene::ID						m_NextSceneID;					//!< 次のSceneIDを格納する変数
	bool							m_IsGameEnd;					//!< ゲームが終了したかどうか

};	// class SceneManager

}	// namespace scene
}	// namespace flying_girl

#endif	// SCENE_MANAGER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
