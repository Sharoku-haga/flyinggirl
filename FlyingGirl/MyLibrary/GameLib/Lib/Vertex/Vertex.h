﻿//==================================================================================================================================//
//!< @file		Vertex.h
//!< @brief		Vertexクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef VERTEX_H
#define VERTEX_H

#include <d3dx9.h>

struct BASIC_VERTEX;

/**頂点情報の基底クラス*/
class Vertex
{

public:
	/**
	* Constructor.
	* @param[in] pD3Device	デバイスへのポインタ
	* @param[in] width		幅
	* @param[in] height		高さ
	* @param[in] deoph		奥行き(デフォルト引数0.0f)
	*/
	Vertex(LPDIRECT3DDEVICE9 pD3Device, float width, float height, float depth = 0.0f);

	/**Destructor*/
	 virtual ~Vertex();

	 /**
	 * 描画関数.
	 * 純粋仮想関数
	 * @param[in] pTexture	テクスチャーへのポインタ
	 * @param[in] posX		X軸の位置座標(デフォルト引数0.0f)		
	 * @param[in] posY		Y軸の位置座標(デフォルト引数0.0f)
	 * @param[in] posZ		Z軸の位置座標(デフォルト引数0.0f)
	 */
	 virtual void Draw(LPDIRECT3DTEXTURE9 pTexture, float posX = 0.0f, float posY = 0.0f, float posZ = 0.0f) = 0;

	/**
	* UVスクロールを行う関数
	* @param[in] scrollSpeedTu	tuのスクロールの速さ(変化量)
	* @param[in] scrollSpeedTv	tvのスクロールの速さ(変化量)
	*/
	void ScrollUV(float scrollSpeedTu, float scrollSpeedTv);

	/**
	* 頂点情報のサイズをセットする関数.
	* @param[in] vtxWidth	頂点情報の幅
	* @param[in] vtxHeight	頂点情報の高さ
	* @param[in] vtxDepth	頂点情報の奥行(デフォルト引数0.0f)
	*/
	void SetSize(float vtxWidth, float vtxHeight, float vtxDepth = 0.0f);

	/**
	* 頂点情報のtu,tv値を設定する関数.
	* @param[in] tuMin tu値の最小値
	* @param[in] tuMax tu値の最大値
	* @param[in] tvMin tv値の最小値
	* @param[in] tvMax tv値の最大値
	*/
	void SetTuTvVal(float tuMin, float tuMax, float tvMin, float tvMax);

	/**
	* 頂点情報の色情報を設定する関数.
	* @param[in] color  色の設定
	*/
	void SetColor(DWORD color);

	/**
	* 頂点情報の横幅のサイズを取得する関数
	* @return m_VtxWidth	頂点情報の横幅
	*/
	inline float GetVtxWidth(){ return m_VtxWidth; }

	/**
	* 頂点情報の縦幅のサイズを取得する関数
	* @return m_VtxHeight	頂点情報の縦幅
	*/
	inline float GetVtxHeight(){ return m_VtxHeight; }

	/**
	* 頂点情報の奥行のサイズを取得する関数
	* @return m_VtxDepth	頂点情報の奥行
	*/
	inline float GetVtxDepth(){ return m_VtxDepth; }

	/**
	* 頂点数を取得する関数
	* @return m_VtxNum		頂点数
	*/
	inline int GetVtxNum(){ return m_VtxNum; }

	/**
	* 頂点情報の先頭アドレスを取得する関数
	* @return 頂点情報
	*/
	inline virtual BASIC_VERTEX*	GetpVertex() = 0;

protected:
	static const int		m_VtxNum = 4;				//!< 頂点数(四角形ポリゴンの為4つ)
	LPDIRECT3DDEVICE9		m_pD3Device;				//!< Direct3Dのデバイス
	float					m_VtxWidth;		  			//!< 横幅
	float					m_VtxHeight;				//!< 縦幅
	float					m_VtxDepth;					//!< 奥行
	float					m_TuMin;					//!< tu最小値
	float					m_TuMax;					//!< tu最大値
	float					m_TvMin;					//!< tv最小値
	float					m_TvMax;					//!< tv最大値
	DWORD                   m_Color[m_VtxNum];			//!< 色の値

	/**頂点情報体を作成する関数*/
	virtual void CreateVtx() = 0;

};


#endif	// VERTEX_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
