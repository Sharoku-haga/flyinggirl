﻿//==================================================================================================================================//
//!< @file		DebugTimer.h
//!< @brief		DebugTimerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef DEBUG_TIMER_H
#define DEBUG_TIMER_H

#include <d3dx9.h>
#include <iostream>
#include <map>
#include <string>

/**
* デバック用時間を計測するクラス<br>
* 時間取得にはtimeGetTimeを使用。他の時間取得関数も検討する
*/
class DebugTimer
{	

public:
	/**
	* Constructor.
	* @param[in] pD3Device	デバイスへのポインタ
	*/
	explicit DebugTimer(LPDIRECT3DDEVICE9 pD3Device);

	/**Destructor*/
	~DebugTimer();

	/**
	* 時間計測をはじめる
	* @param[in] rTimeName 計測をはじめる時間の名前
	*/
	void Start(const std::string&  rTimeName);

	/**
	* 時間計測終了
	* @param[in] rTimeName 計測を終える時間の名前
	*/
	void End(const std::string&  rTimeName);

	/**
	* 時間計測結果を取得する
	* @param[in] rTimeName 計測結果を取得したい時間の名前
	*/
	DWORD GetMeasuringResult(const std::string&  rTimeName);

	/**
	* 1つの計測結果だけ描画する
	* @param[in] rTimeName 結果を表示したい計測時間の名前
	* @param[in] rPos 表示する座標
	*/
	void DrawMeasuringResult(const std::string&  rTimeName, const D3DXVECTOR2& rPos);

	/**
	* 合計結果を描画する関数
	* @param[in] rPos 表示する座標
	*/
	void DrawTotalResult(const D3DXVECTOR2& rPos);

	/**
	* すべての計測結果と合計結果を描画する
	* @param[in] rPos 表示開始座標
	*/
	void DrawAllResult(const D3DXVECTOR2 rPos);

private:
	LPDIRECT3DDEVICE9					m_pD3Device;		//!< デバイスへのポインタ
	std::map<std::string, DWORD>		m_DebugTime;		//!< デバックタイムを格納するmap

};

#endif // DEBUG_TIMER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
