﻿//==================================================================================================================================//
//!< @file		VertexManager.cpp
//!< @brief		VertexManagerクラス実装
//!< @author	haga
//==================================================================================================================================//

//---------------------------------------------------------------------------------------------------------------//
//Includes
//---------------------------------------------------------------------------------------------------------------//

#include "VertexManager.h"
#include "Vertex.h"
#include "Vertex2D.h"
#include "Vertex2DCenterPos.h"
#include "Vertex3D.h"
#include "Vertex3DCenterPos.h"
#include "Vertex3Dxz.h"
#include "Vertex3DxzCenterPos.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

VertexManager::VertexManager(LPDIRECT3DDEVICE9 pD3Device)
	: m_pD3Device(pD3Device)
{}

VertexManager::~VertexManager()
{
	ReleaseALL();
}

// 頂点情報(2D空間)を作成し、登録する関数.
int VertexManager::CreateVtx2D(float width, float height)
{
	int key = m_pVertex.size();
	m_pVertex.emplace_back(new Vertex2D(m_pD3Device, width, height));
	return key;
}

// 位置座標が中心の頂点情報(2D空間)を作成し、登録する関数.
int VertexManager::CreateVtx2DCenterPos(float width, float height)
{
	int key = m_pVertex.size();
	m_pVertex.emplace_back(new Vertex2DCenterPos(m_pD3Device, width, height));
	return key;
}

// 頂点情報(3D空間)を作成し、登録する関数
int VertexManager::CreateVtx3D(float width, float height, float depth)
{
	int key = m_pVertex.size();
	m_pVertex.emplace_back(new Vertex3D(m_pD3Device, width, height, depth));
	return key;
}

// 位置座標が中心の頂点情報(3D空間)を作成し、登録する関数
int VertexManager::CreateVtx3DCenterPos(float width, float height, float depth)
{
	int key = m_pVertex.size();
	m_pVertex.emplace_back(new Vertex3DCenterPos(m_pD3Device, width, height, depth));
	return key;
}

// XZ平面の頂点情報(3D空間)を作成し、登録する関数
int VertexManager::CreateVtx3Dxz(float width, float height, float depth)
{
	int key = m_pVertex.size();
	m_pVertex.emplace_back(new Vertex3Dxz(m_pD3Device, width, height, depth));
	return key;
}

// 位置座標が中心のXZ平面の頂点情報(3D空間)を作成し、登録する関数
int VertexManager::CreateVtx3DxzCenterPos(float width, float height, float depth)
{
	int key = m_pVertex.size();
	m_pVertex.emplace_back(new Vertex3DxzCenterPos(m_pD3Device, width, height, depth));
	return key;
}

// 通常描画
void VertexManager::Draw(int key, LPDIRECT3DTEXTURE9 pTexture, float posX, float posY, float posZ)
{
	m_pVertex[key]->Draw(pTexture, posX, posY, posZ);
}

// サイズを変更する関数
void VertexManager::SetSize(int key, float width, float height, float depth)
{
	m_pVertex[key]->SetSize(width, height, depth);
}

// tu値とtv値を変更する関数
void VertexManager::SetTuTv(int key, float tuMin, float tuMax, float tvMin, float tvMax)
{
	m_pVertex[key]->SetTuTvVal(tuMin, tuMax, tvMin, tvMax);
}

// 色を変更する関数
void VertexManager::SetColor(int key, DWORD color)
{
	m_pVertex[key]->SetColor(color);
}

// UVスクロールを行う関数
void VertexManager::ScrollUV(int key, float scrollSpeedTu, float scrollSpeedTv)
{
	m_pVertex[key]->ScrollUV(scrollSpeedTu, scrollSpeedTv);
}

// 横幅を取得する関数
float VertexManager::GetWidth(int key)
{
	return m_pVertex[key]->GetVtxWidth();
}

// 縦幅を取得する関数
float VertexManager::GetHeight(int key)
{
	return m_pVertex[key]->GetVtxHeight();
}

// 奥行を取得する関数
float VertexManager::GetDepth(int key)
{
	return m_pVertex[key]->GetVtxDepth();
}

// 頂点情報を解放する関数
void VertexManager::Release(int key)
{
	delete m_pVertex[key];
	m_pVertex[key] = NULL;
}

// 全ての頂点情報を解放する関数
void VertexManager::ReleaseALL()
{
	for(auto& pVertex :m_pVertex)
	{
		if(pVertex != nullptr)
		{
			delete pVertex;
			pVertex = nullptr;
		}
	}

	std::vector<Vertex*>().swap(m_pVertex);
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
