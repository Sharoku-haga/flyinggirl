﻿//==================================================================================================================================//
//!< @file		TitleBackground.cpp
//!< @brief		TitleBackgroundクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "TitleBackground.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace title
{

/* Public Functions ------------------------------------------------------------------------------------------- */

TitleBackground::TitleBackground(int texID)
	: m_Pos({0.0f, 0.0f})
	, m_TexID(texID)
	, m_rGameLib(GameLib::Instance())
{
	// ウィンドウの大きさを取得し、それをもとにVetexを作成する
	float width		= static_cast<float>(m_rGameLib.GetWindowWidth());
	float height	= static_cast<float>(m_rGameLib.GetWindowHeight());
	m_VtxID			= m_rGameLib.CreateVtx2D(width, height);
}

TitleBackground::~TitleBackground()
{
	m_rGameLib.ReleaseVtx(m_VtxID);
	// Textureは読み込んだところで解放する
}

void TitleBackground::Draw()
{
	m_rGameLib.Draw(m_TexID, m_VtxID, m_Pos.x, m_Pos.y);
}

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
