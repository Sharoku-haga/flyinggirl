﻿//==================================================================================================================================//
//!< @file		BasicButton.h
//!< @brief		BasicButtonクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef BASIC_BUTTON_H
#define BASIC_BUTTON_H

#include <d3dx9.h>
#include "Button.h"

namespace flying_girl
{
namespace btn
{

//======================================================================//
//!< ボタンの描画だけを行うクラス
//======================================================================//
class BasicButton : public Button
{

public:
	/** 
	* Constructor
	* @param[in] pos			位置座標
	* @param[in] texID			TextureID
	* @param[in] vtxID			VertexID
	* @param[in] vtxWidth		ボタンのVertexの横幅
	* @param[in] vtxHeight		ボタンのVertexの縦幅
	*/
	BasicButton(D3DXVECTOR2 pos, int texID, int vtxID, float vtxWidth, float vtxHeight);

	/** Destructor */
	virtual ~BasicButton();

private:
	D3DXVECTOR2			m_Pos;			//!< 位置座標
	int					m_TexID;		//!< TextureID
	int					m_VtxID;		//!< VertexID
	float				m_VtxWidth;		//!< ボタンのVertexの横幅
	float				m_VtxHeight;	//!< ボタンのVertexの縦幅

	/** 処理実行関数 */
	virtual void	Run()override;

	/** 描画処理関数 */
	virtual void	Render()override;

	/**
	* VetexIDを取得する関数
	* @return VertexID
	*/
	virtual int		GetVertexID()override{ return m_VtxID; }

};

}	// namespace btn
}	// namespace flying_girl

#endif	// BASIC_BUTTON_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
