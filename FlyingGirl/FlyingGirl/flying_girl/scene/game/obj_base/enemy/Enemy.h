﻿//==================================================================================================================================//
//!< @file		Enemy.h
//!< @brief		Enemyクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef ENEMY_H
#define ENEMY_H

#include "../ObjBase.h"

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace obj
{
namespace enemy
{

//======================================================================//
//!< 敵の基底クラス
//======================================================================//
class Enemy : public ObjBase
{

public:
	/**
	* Constructor
	* @param[in] pos	位置座標
	* @param[in] texID	テクスチャーID
	*/
	Enemy(D3DXVECTOR2 pos, int texID);

	/** Destructor */
	virtual ~Enemy() = default;

protected:
	/** 処理実行関数 */
	virtual void Run() = 0;

	/** 描画関数 */
	virtual void Render() = 0;

	/** 衝突判定処理を行う関数 */
	virtual void ExecuteCollisionProcessing() = 0;


};	// class Enemy

}	// namespace enemy
}	// namespace obj
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// ENEMY_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
