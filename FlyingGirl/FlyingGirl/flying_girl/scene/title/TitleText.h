﻿//==================================================================================================================================//
//!< @file		TitleText.h
//!< @brief		TitleTextクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef TITLE_TEXT_H
#define TITLE_TEXT_H

#include <d3dx9.h>

class GameLib;

namespace flying_girl
{
namespace scene
{
namespace title
{

//======================================================================//
//!< TitleSceneのタイトル文字を描画するクラス
//======================================================================//

class TitleText
{

public:
	/** 
	* Constructor
	* @param[in] texID	TextureID
	*/
	explicit TitleText(int texID);

	/** Destructor */
	~TitleText();

	/** 
	* コントロール関数
	* @return フェードインが終わったかどうか
	*/
	bool Control();

	/** 描画関数 */
	void Draw();

private:
	D3DXVECTOR2			m_Pos;					//!< 位置座標	
	int					m_TexID;				//!< TextureID
	int					m_VtxID;				//!< VertexID
	BYTE				m_CurrentAlpha;			//!< 現在のアルファ値
	bool				m_IsFadeInEnd;			//!< フェードインが終わったかどうかのフラグ

	GameLib&			m_rGameLib;				//!< GameLibクラス(Library)のインスタンスへの参照

};	// class TitleText 

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

#endif	// TITLE_TEXT_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
