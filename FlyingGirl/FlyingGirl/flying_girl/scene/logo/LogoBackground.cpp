﻿//==================================================================================================================================//
//!< @file		LogoBackground.cpp
//!< @brief		LogoBackgroundクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "LogoBackground.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace logo
{

/* Unnamed Namespace ------------------------------------------------------------------------------------------ */

namespace
{

const BYTE		AlphaMinVal		= 0x00;		// アルファ値の最小値
const BYTE		AlphaMaxVal		= 0xff;		// アルファ値の最大値
const BYTE		FadeSpeed		= 0x05;		// フェードのスピード(アルファ値)
const BYTE		ColorDefaultVal = 0xff;		// カラーの設定するときのデフォルト値
const int		DisplayTime		= 120;		// ロゴ表示時間. 60FPSで計算

}

/* Public Functions ------------------------------------------------------------------------------------------- */

LogoBackground::LogoBackground(int texID)
	: m_Pos({0.0f, 0.0f})
	, m_TexID(texID)
	, m_DisplayTimeCount(0)
	, m_State(FADE_IN_LOGO)
	, m_CurrentAlpha(AlphaMinVal)
	, m_IsFadeOutLogo(false)
	, m_rGameLib(GameLib::Instance())
{
	// ウィンドウの大きさを取得し、それをもとにVetexを作成する
	float width		= static_cast<float>(m_rGameLib.GetWindowWidth());
	float height	= static_cast<float>(m_rGameLib.GetWindowHeight());
	m_VtxID			= m_rGameLib.CreateVtx2D(width, height);
}

LogoBackground::~LogoBackground()
{
	m_rGameLib.ReleaseVtx(m_VtxID);
	// Textureの解放はリTextureを読み込んだ場所で行う
}

bool LogoBackground::Control()
{
	switch(m_State)
	{
	case FADE_IN_LOGO:		
		if(m_CurrentAlpha == AlphaMaxVal)
		{	
			m_State = DISPLAY_LOGO;
		}
		else
		{
			m_CurrentAlpha += FadeSpeed;
		}
		break;

	case DISPLAY_LOGO:		
		if(m_DisplayTimeCount == DisplayTime)
		{	
			m_State = FADE_OUT_LOGO;
		}
		else
		{
			++m_DisplayTimeCount;
		}
		break;

	case FADE_OUT_LOGO:		
		if(m_CurrentAlpha == AlphaMinVal)
		{	
			m_IsFadeOutLogo = true;
		}
		else
		{
			m_CurrentAlpha -= FadeSpeed;
		}
		break;

	default:
		// do nothing
		break;
	}

	return m_IsFadeOutLogo;
}

void LogoBackground::Draw()
{
	m_rGameLib.SetVtxColor(m_VtxID, D3DCOLOR_ARGB(m_CurrentAlpha, ColorDefaultVal, ColorDefaultVal, ColorDefaultVal));
	m_rGameLib.Draw(m_TexID, m_VtxID, m_Pos.x, m_Pos.y);
}

}	// namespace logo
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
