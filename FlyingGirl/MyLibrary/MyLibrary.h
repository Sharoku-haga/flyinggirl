﻿//==================================================================================================================================//
//!< @file			MyLibrary.h
//!< @brief			MyLibraryのヘッダーファイルをまとめたヘッダ
//!< @author		haga
//!< @attention		あくまでこのヘッダーは確認用なのでincludeはしないこと
//==================================================================================================================================//

#define MY_LIBRARY_H

#ifndef MY_LIBRARY_H
#define MY_LIBRARY_H

/*
設定するLib
#pragma comment (lib,"winmm.lib")
#pragma comment (lib,"d3d9.lib")
#pragma comment (lib,"d3dx9.lib")
#pragma comment (lib,"dxguid.lib")
#pragma comment (lib,"shlwapi.lib")
#pragma comment (lib,"dinput8.lib")
#pragma comment (lib,"Dsound.lib")
#pragma comment (lib,"MyLibrary.lib")
*/

//------------------------------------------------------------------------------------------//
//					MyLibrary内の主なヘッダーファイル
//------------------------------------------------------------------------------------------//

#include "GameLib/GameLib.h"									//!< Facadeパターンのライブラリ
#include "Font/Font.h"											//!< フォントクラス
#include "Camera/Camera.h"										//!< カメラクラス
#include "Light/Light.h"										//!< ライトクラス

//------------------------------------------------------------------------------------------//
//				以下はGameLibのLibファルダ内のヘッダーファイル
//------------------------------------------------------------------------------------------//

//--Windowフォルダ--------------------------------------------------------------------------//

#include "GameLib/Lib/Window/WindowCreator.h"					//!< Window関連クラス

//--Graphicsフォルダ------------------------------------------------------------------------//

#include "GameLib/Lib/Graphics/GraphicsDevice.h"				//!< GraphicsDeviceのクラス
#include "GameLib/Lib/Graphics/TextureManager.h"				//!< テクスチャー管理クラス
#include "GameLib/Lib/Graphics/XFileManager.h"					//!< Xファイル管理クラス
#include "GameLib/Lib/Graphics/XFile.h"							//!< Xファイルクラス

//--Veretxフォルダ--------------------------------------------------------------------------//

#include "GameLib/Lib/Veretex/Vertex.h"							//!< 頂点情報の基底クラス
#include "GameLib/Lib/Veretex/Vertex2D.h"						//!< 2D頂点情報クラス
#include "GameLib/Lib/Veretex/Vertex3D.h"						//!< 3D頂点情報(XY平面)クラス
#include "GameLib/Lib/Veretex/Vertex3Dxz.h"						//!< 3D頂点情報(XZ平面)クラス
#include "GameLib/Lib/Veretex/VertexManager.h"					//!< 頂点管理クラス

//--Animationフォルダ-----------------------------------------------------------------------//

#include "GameLib/Lib/Animation/Animation.h"					//!< アニメーション情報クラス
#include "GameLib/Lib/Animation/AnimationManager.h"				//!< アニメーション情報管理クラス

//--DirectInputフォルダ---------------------------------------------------------------------//

#include "GameLib/Lib/DirectInput/InputDevice.h"				//!< DirectInputデバイス関連クラス
#include "GameLib/Lib/DirectInput/InputKey.h"					//!< キーボード操作関連クラス
#include "GameLib/Lib/DirectInput/InputMouse.h"					//!< マウス操作関連クラス

//--Soundフォルダ---------------------------------------------------------------------------//

#include "GameLib/Lib/Sound/SoundFileManager.h"					//!< サウンド管理クラス

//--DebugToolフォルダ-----------------------------------------------------------------------//

#include "GameLib/Lib/DebugTool/DebugFont.h"					//!< デバックフォントクラス
#include "GameLib/Lib/DebugTool/DebugTimer.h"					//!< デバックタイマークラス

#endif // MY_LIBRARY_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
