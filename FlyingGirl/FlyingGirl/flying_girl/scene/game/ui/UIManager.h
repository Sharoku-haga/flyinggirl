﻿//==================================================================================================================================//
//!< @file		UIManager.h
//!< @brief		UIManagerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef UI_MANAGER_H
#define UI_MANAGER_H

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace ui
{

class UIManager
{

public:
	/** Constructor */
	UIManager();

	/** Destructor */
	~UIManager();

};	// class UIManager

}	// namespace ui
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// UI_MANAGER_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
