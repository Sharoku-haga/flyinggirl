﻿//==================================================================================================================================//
//!< @file		ScoreWindow.h
//!< @brief		ScoreWindowクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

/** @todo 2017/02/24現在 ウィンドウを表示するだけしか実装していない */

#ifndef SCORE_WINDOW_H
#define SCORE_WINDOW_H

#include <d3dx9.h>

class GameLib;

namespace flying_girl
{

namespace score
{

class ScoreDataManager;

}	// namespace score

namespace scene
{
namespace title
{

//======================================================================//
//!< TitleSceneでスコアを表示するウインドウのクラス
//======================================================================//
class ScoreWindow
{

public:
	/** 
	* Constructor
	* param[in] pScoreDataManager	ScoreDataManagerクラスのインスタンスへのポインタ
	* param[in] texID				TextureID
	*/
	ScoreWindow(score::ScoreDataManager* pScoreDataManager, int texID);

	/** Destructor */
	~ScoreWindow();

	/**
	* コントロール関数
	* @retrurn Menu選択に戻るかどうか
	*/
	bool Control();

	/** 描画関数 */
	void Draw();

private:
	score::ScoreDataManager*		m_pScoreDataManager;		//!< ScoreDataManagerクラスのインスタンスへのポインタ
	D3DXVECTOR2						m_Pos;						//!< 位置座標
	int								m_TexID;					//!< TexTureID	
	int								m_VtxID;					//!< VertexID
	bool							m_IsDisplayEnd;				//!< スコアウィンドウ表示が終わったかどうかのフラグ

	GameLib&						m_rGameLib;					//!< GameLibクラス(Library)のインスタンスへの参照


};	// class ScoreWindow

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

#endif	// SCORE_WINDOW_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
