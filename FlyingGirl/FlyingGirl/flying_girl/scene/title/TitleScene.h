﻿//==================================================================================================================================//
//!< @file		TitleScene.h
//!< @brief		TitleSceneクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef TITLE_SCENE_H
#define TITLE_SCENE_H

#include "../Scene.h"

namespace flying_girl
{

namespace score
{

class ScoreDataManager;

}	// namespace score

namespace scene
{
namespace title
{

class TitleBackground;
class TitleText;
class TitleMenu;
class ScoreWindow;

//======================================================================//
//!< タイトルやメニューを表示し、他のSceneへの遷移などを行うクラス
//======================================================================//
class TitleScene : public Scene
{

public:
	/** TitleSceneの状態 */
	enum STATE
	{
		FADE_IN_TEXT,		//!< タイトルテキストがフェードインしている
		SELECT_MENU,		//!< メニュー選択
		DISPLAY_SCORE,		//!< スコア表示
		GAME_START,			//!< ゲーム開始
		GAME_END,			//!< ゲーム終了
	};

	/** 
	* Constructor
	* @param[in] pScoreDataManager		ScoreDataManagerクラスのインスタンスへのポインタ
	*/
	explicit TitleScene(score::ScoreDataManager* pScoreDataManager);

	/** Desutructor */
	virtual ~TitleScene();

private:
	TitleBackground*		m_pBackground;			//!< TitleBackgroundクラスのインスタンスへのポインタ
	TitleText*				m_pTitleText;			//!< TitleTextクラスのインスタンスへのポインタ
	TitleMenu*				m_pMenu;				//!< TitleMenuクラスのインスタンスへのポインタ
	ScoreWindow*			m_pScoreWindow;			//!< ScoreWindowクラスのインスタンスへのポインタ
	STATE					m_State;				//!< TitleSceneの状態を格納している

	/**
	* コントロール関数.
	* @return	次のSceneID
	*/
	virtual Scene::ID	Control()override;

	/** 描画関数 */
	virtual void		Draw()override;

};	// class TitleScene

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

#endif	// TITLE_SCENE_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
