﻿//==================================================================================================================================//
//!< @file		Animation.h
//!< @brief		Animationクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef ANIMATION_H
#define ANIMATION_H

#include <vector>

/**
* アニメーションの情報を管理するクラス
*/
class Animation
{

public:
	/**アニメーションUV管理用構造体*/
	struct AnimeUV
	{
		float		m_MinTu;				//!< 最小tu値
		float		m_MinTv;				//!< 最小tv値
		float       m_MaxTu;				//!< 最大tu値
		float       m_MaxTv;				//!< 最大tv値
		/**初期化用Constructor*/
		AnimeUV(float minTu, float maxTu, float minTv, float maxTv)
			: m_MinTu(minTu)
			, m_MaxTu(maxTu)
			, m_MinTv(minTv)
			, m_MaxTv(maxTv)
		{}

		AnimeUV(){}
	};

	/**
	* Constructor
	* @param[in]	tuCount		Tu方向アニメーションのカウント数
	* @param[in]	tvCount		Tv方向アニメーションのカウント数
	* @param[in]	interval	アニメーションの時間間隔	
	*/
	Animation(int tuCount, int tvCount, int interval);

	/**Desutructor*/
	~Animation();

	/**
	* アニメの情報をセットする関数
	* @param[in] animeNum	アニメ番号
	* @param[in] minTu		セットするTu最小値
	* @param[in] maxTu		セットするTu最大値
	* @param[in] minTv      セットするTv最小値
	* @param[in] maxTv		セットするTv最大値
	*/
	void SetAnimeInfo(int animeNum, float minTu, float maxTu, float minTv, float maxTv);

	/**
	* アニメーションのUV情報を取得する関数
	* @param[in]	animeNum	アニメーション番号
	* @return	番号に応じたアニメーションのUV情報を取得する関数
	*/
	const Animation::AnimeUV&	GetAnimeUV(int animeNum){ return m_AnimeUV[animeNum]; }

	/**
	* アニメーション間隔を取得する関数
	* @return アニメーション間隔
	*/
	int GetInterval(){ return m_Interval; }

	/**
	* アニメーションパターン数を取得する関数
	* @return	 アニメーションパターン数
	*/
	int GetAnimePattern(){ return static_cast<int>(m_AnimeUV.size()); }

private:
	std::vector<AnimeUV>	m_AnimeUV;			//!< アニメTV情報を格納する変数の動的配列	
	int						m_Interval;			//!< アニメ間隔

	/**
	* 初期化関数
	* @param[in] tuCount	Tu方向アニメーションのカウント数
	* @param[in] tvCount	Tv方向アニメーションのカウント数
	*/
	void Init(int tuCount, int tvCount);
		
};

#endif // ANIMATION_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
