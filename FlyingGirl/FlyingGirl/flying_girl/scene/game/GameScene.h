﻿//==================================================================================================================================//
//!< @file		GameScene.h
//!< @brief		GameSceneクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef GAME_SCENE_H
#define GAME_SCENE_H

#include "../Scene.h"

namespace flying_girl
{
namespace scene
{
namespace game
{

//======================================================================//
//!< シューティングゲームを行うSceneクラス
//======================================================================//
class GameScene : public Scene
{

public:
	/** Constructor */
	GameScene();

	/** Destructor */
	~GameScene();

private:
	/**
	* コントロール関数.
	* @return	次のSceneID
	*/
	virtual Scene::ID	Control()override;

	/** 描画関数 */
	virtual void		Draw()override;

};	// class GameScene

}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// GAME_SCENE_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
