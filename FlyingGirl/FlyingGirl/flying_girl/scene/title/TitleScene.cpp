﻿//==================================================================================================================================//
//!< @file		TitleScene.cpp
//!< @brief		TitleSceneクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "TitleScene.h"
#include "GameLib/GameLib.h"
#include "TitleBackground.h"
#include "TitleText.h"
#include "TitleMenu.h"
#include "ScoreWindow.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace title
{

/* Public Functions ------------------------------------------------------------------------------------------- */

TitleScene::TitleScene(score::ScoreDataManager* pScoreDataManager)
	: m_pBackground(nullptr)
	, m_pTitleText(nullptr)
	, m_pMenu(nullptr)
	, m_pScoreWindow(nullptr)
	, m_State(FADE_IN_TEXT)
{
	// 背景作成
	{
		int texID = m_rGameLib.LoadTex("../Resource/title/TitleBackground.png");
		m_pBackground = new TitleBackground(texID);
	}

	// タイトルテキスト作成
	{
		int texID = m_rGameLib.LoadTex("../Resource/title/TitleText.png");
		m_pTitleText = new TitleText(texID);
	}

	// メニュー作成
	{
		int texID = m_rGameLib.LoadTex("../Resource/title/TitleButton.png");
		m_pMenu = new TitleMenu(texID);
	}

	// スコアウィンドウ作成
	{
		int texID = m_rGameLib.LoadTex("../Resource/title/ScoreWindow.png");
		m_pScoreWindow = new ScoreWindow(pScoreDataManager, texID);
	}
}

TitleScene::~TitleScene()
{
	delete m_pScoreWindow;
	m_pScoreWindow = nullptr;

	delete m_pMenu;
	m_pMenu = nullptr;

	delete m_pTitleText;
	m_pTitleText = nullptr;

	delete m_pBackground;
	m_pBackground = nullptr;

	m_rGameLib.ReleaseVtxALL();
	m_rGameLib.ReleaseTexALL();
}

/* Private Functions ------------------------------------------------------------------------------------------ */

Scene::ID TitleScene::Control()
{
	switch(m_State)
	{
	case FADE_IN_TEXT:
		if(m_pTitleText->Control())
		{
			m_State = SELECT_MENU;
		}
		else
		{
			// テスト用処理. フェードアウト処理が終わっていない場合
		}
		break;

	case SELECT_MENU:
		m_State = m_pMenu->Control();
		break;

	case DISPLAY_SCORE:
		if(m_pScoreWindow->Control())
		{
			m_State = SELECT_MENU;
		}
		else
		{
			// テスト用処理. スコアが表示が継続している場合
		}
		break;

	case GAME_START:
		return Scene::ID::GAME;
		break;

	case GAME_END:
		return Scene::ID::GAME_END;
		break;

	default:
		// do nothing
		break;
	}
	return Scene::ID::TITLE;
}

void TitleScene::Draw()
{
	m_pBackground->Draw();
	m_pTitleText->Draw();

	switch(m_State)
	{
	case FADE_IN_TEXT:
		// Menuを描画させない為このcaseを記載
		break;

	case DISPLAY_SCORE:
		m_pScoreWindow->Draw();
		break;

	default:	// SELECT_MENU, GAME_START, GAME_ENDの3つのどれかの場合の処理  
		m_pMenu->Draw();
		break;
	}
}

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
