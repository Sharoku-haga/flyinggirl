﻿//==================================================================================================================================//
//!< @file		TextureManager.h
//!< @brief		TextureManagerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <d3dx9.h>
#include <d3d9.h>
#include <vector>

/**テクスチャーを管理するクラス*/
class TextureManager
{

public:
	/**
	* Constructor
	* @param[in] pD3Device	デバイスへのポインタ
	*/
	explicit TextureManager(LPDIRECT3DDEVICE9 pD3Device);

	/**Destructor*/
	~TextureManager();

	/**
	* テクスチャーを読み込む関数.
	* @param[in] pFilepath   ファイルパス
	* @return	登録したキーを返す
	*/
	int Load(TCHAR* pFilePath);

	/**
	* テクスチャーを詳細設定して読み込む関数.
	* @param[in] pFilepath		ファイルパス
	* @param[in] alpha			アルファ値
	* @param[in] red			色のR値 255
	* @param[in] green			色のG値 255
	* @param[in] blue			色のB値 255
	* @param[in] IsPowerTwo		テクスチャーのサイズが２のべき乗ならtrue,違うならfalse(デフォルト引数:false)
	*/
	int LoadEx(TCHAR* pFilePath, int alpha, int red, int green, int blue, bool IsPowerTwo = false);

	/**
	* テクスチャーのアドレスを取得する関数
	* @param[in] key	登録したキー
	*/
	inline LPDIRECT3DTEXTURE9 GetpTexture(int key){ return m_pTextures[key]; }

	/**
	* 登録しているテクスチャー解放関数
	* @param[in] key	登録したキー
	*/
	void Release(int key);

	/**
	* 登録しているテクスチャーをすべて解放する関数
	*/
	void ReleaseALL();

private:
	LPDIRECT3DDEVICE9						m_pD3Device;		//!< デバイスへのポインタ
	std::vector<LPDIRECT3DTEXTURE9>			m_pTextures;		//!< テクスチャーへのポインタを格納する動的配列

};

#endif	// TEXTURE_MANAGER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
