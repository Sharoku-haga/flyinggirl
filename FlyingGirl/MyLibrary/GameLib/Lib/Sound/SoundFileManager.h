﻿//==================================================================================================================================//
//!< @file		SoundFileManager.h
//!< @brief		SoundFileManagerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef SOUND_FILE_MANAGER_H
#define SOUND_FILE_MANAGER_H

#include <windows.h>
#include <mmsystem.h>
#include <vector>
#include <dsound.h>

/**音楽の再生方式*/
enum SOUND_MODE
{
	PLAY,			//!< 再生
	PLAYLOOP,		//!< ループ再生
	STOP,			//!< 停止
	RESET,			//!< 初期位置に戻す
	STOP_RESET,		//!< 停めて初期位置戻す
	RESET_PLAY		//!< 初期位置から再生し直す
};

/**
* サウンドを管理するクラス
*/
class SoundFileManager
{

public:
	/**Constructor<br>*/
	SoundFileManager();

	/**Destructor*/
	~SoundFileManager();
	
	/**
	* DirectSound初期化する関数
	* @param[in] hWnd ウィンドウハンドル
	*/
	void InitSound(HWND hWnd);

	/**
	* 音楽を読み込む関数.
	* @param[in]	pFilepath	音楽ファイル名
	* @return		登録したキー もし読み込みに失敗した場合はINT_MAXが返ってくる
	*/
	int LoadSound(TCHAR* pFilePath);

	/**
	* 音楽を再生する関数
	* @param[in]  key			登録したキー
	* @param[in] mode			音楽の再生方式
	*/
	void SoundPlayer(int key, SOUND_MODE sMode); 

	/**
	* 登録しているサウンドを解放関数
	* @param[in] key 登録したキー
	*/
	void Release(int key);

	/**登録しているサウンド全てを解放する関数*/
	void ReleaseALL();

private:
	IDirectSound8*								m_pDSound8;			//!< DirectSoundのインターフェイス
	std::vector<LPDIRECTSOUNDBUFFER8>			m_Sounds;			//!< サウンドを格納する動的配列

	/**
	* WAVEファイルオープン関数.
	* @param[in]	filepath		音楽ファイル名
	* @param[out]   waveFormatEx	waveフォーマット
	* @param[out]	pwaveData		waveデータ
	* @param[out]	dataSize		データサイズ
	* @retval	true	waveファイルオープン成功
	* @retval	false	waveファイルオープン失敗
	*/
	bool OpenWave(TCHAR* filepath, WAVEFORMATEX* waveFormatEx, char** pwaveData, DWORD* dataSize);

};

#endif	// SOUND_FILE_MANAGER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
