﻿//==================================================================================================================================//
//!< @file		Main.cpp
//!< @brief		少女大飛行のMain.cpp
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include <windows.h>
#include <crtdbg.h>
#include "Icon/resource.h"
#include "GameLib/GameLib.h"
#include "flying_girl/scene/SceneManager.h"

/* Defins ----------------------------------------------------------------------------------------------------- */

//#define FULL_SCREEN_MODE					// フルスクリーンモード
#define GAME_TITLE TEXT("少女大飛行")		// ゲームタイトル
#define CLIENT_W 1280						// クライアント領域の幅
#define CLIENT_H 960						// クライアント領域の高さ
#define GAME_FPS (1000 / 60)				// FPS

/* Function Prototype ----------------------------------------------------------------------------------------- */

// ウインドウプロシージャ
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

/* Entry Point ------------------------------------------------------------------------------------------------ */

// エントリポイント
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	MSG msg;

	/* Initialize Library ------------------------------------------------------------------------------- */

	HRESULT hr;
#ifdef FULL_SCREEN_MODE
	hr = GameLib::Instance().InitGameLib(GAME_TITLE, CLIENT_W, CLIENT_H, WindowProc, true, true, IDI_ICON1);
#else
	hr = GameLib::Instance().InitGameLib(GAME_TITLE, CLIENT_W, CLIENT_H, WindowProc, false, true, IDI_ICON1);
#endif

	if(FAILED(hr))
	{
		MessageBox(0, "Gameの初期化に失敗しました。", NULL, MB_OK);
		return E_FAIL;
	}
	else
	{
		// テスト用空処理. 初期化成功した場合この処理に入る
	}

	/* Roop Game ---------------------------------------------------------------------------------------- */
	
	// SceneManagerを生成する
	flying_girl::scene::SceneManager* pSceneManager = nullptr;
	pSceneManager = new  flying_girl::scene::SceneManager();

	DWORD currentTime;						// 現在の時間
	DWORD oldTime = timeGetTime();			// 前の時間

	ZeroMemory(&msg, sizeof(msg));
	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			DispatchMessage(&msg);
		}
		else
		{
			currentTime = timeGetTime();
			if(currentTime - oldTime >= GAME_FPS)
			{
				if(pSceneManager->Update())
				{
					break;
				}
				else
				{
					// テスト用空処理. ゲームプログラムが終了していない場合にこの処理に入る
				}
				oldTime = timeGetTime();
			}
			else
			{
				// テスト用空処理. 60FPSたっていない場合にこの処理に入る
			}
		}
	}

	delete pSceneManager;
	pSceneManager = nullptr;

	GameLib::Instance().ReleaseGameLib();	// ライブラリ内のメモリ開放

	return (INT)msg.wParam;
}

/* Functions -------------------------------------------------------------------------------------------------- */

// ウインドウプロシージャ関数
LRESULT CALLBACK WindowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch(message)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return  0;
	}
	break;
	case WM_KEYDOWN:
		switch((CHAR)wparam)
		{
		case VK_ESCAPE:
		{
			int answer = MessageBox(hwnd, TEXT("終わりますか？"), TEXT("終了"), MB_YESNO);
			if(answer == IDYES)
			{
				PostQuitMessage(0);
				return 0;
			}
		}

		break;
		}
		break;

	case WM_SYSKEYDOWN:     // Alt + 特殊キーの処理に使う
		switch((CHAR)wparam)
		{
		case VK_RETURN:     // Alt + Enterを押すとウィンドウ切り替え
			GameLib::Instance().ChangeWindowMode();
			break;
		default:
			break;
		}
		break;

	case WM_ACTIVATE:
		switch((CHAR)wparam)
		{
		case WA_ACTIVE:
		case WA_CLICKACTIVE:

			break;
		}
	}
	return  DefWindowProc(hwnd, message, wparam, lparam);
}

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
