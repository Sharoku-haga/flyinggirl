﻿//==================================================================================================================================//
//!< @file		Vertex3Dxz.h
//!< @brief		Vertex3Dxzクラスヘッダ
//!< @auhtor	haga
//==================================================================================================================================//

#ifndef VERTEX_3D_XZ_H
#define VERTEX_3D_XZ_H

#include "Vertex.h"
#include "CustomVertex.h"

/**
* 3Dのおける頂点情報(XZ平面)を管理、描画するクラス
* 位置座標は左上
*/
class Vertex3Dxz : public Vertex
{

public:
	/**
	* Constructor
	* @param[in] pD3Device	デバイスへのポインタ
	* @param[in] width		幅
	* @param[in] height		高さ
	* @param[in] depth		奥行
	*/
	Vertex3Dxz(LPDIRECT3DDEVICE9 pD3Device, float width, float height, float depth);

	/**Destructor*/
	virtual ~Vertex3Dxz();

	/**
	* 描画関数(XZ平面).
	* @param[in] pTexture	テクスチャーへのポインタ
	* @param[in] posX		X軸の位置座標(デフォルト引数0.0f)
	* @param[in] posY		Y軸の位置座標(デフォルト引数0.0f)
	* @param[in] posZ		Z軸の位置座標(デフォルト引数0.0f)
	*/
	virtual void Draw(LPDIRECT3DTEXTURE9 pTexture, float posX = 0.0f, float posY = 0.0f, float posZ = 0.0f);

	/**
	* 頂点情報の先頭アドレスを取得する関数
	* @return 頂点情報
	*/
	inline virtual BASIC_VERTEX*	GetpVertex(){ return m_Vtx; }

private:
	CUSTOM_VERTEX_3D			m_Vtx[m_VtxNum];				//!< 頂点情報配列

	/**頂点情報体を作成する関数*/
	virtual void CreateVtx();

};

#endif	// VERTEX_3D_XZ_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
