﻿//==================================================================================================================================//
//!< @file		Vertex.cpp
//!< @brief		Vertexクラス実装
//!< @author	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "Vertex.h"
#include "CustomVertex.h"

//--------------------------------------------------------------------------------------------------------------//
//Static variable
//--------------------------------------------------------------------------------------------------------------//

const int	Vertex::m_VtxNum;

//--------------------------------------------------------------------------------------------------------------//
//Public functions 
//--------------------------------------------------------------------------------------------------------------//

Vertex::Vertex(LPDIRECT3DDEVICE9 pD3Device, float width, float height, float depth)
	: m_pD3Device(pD3Device)
	, m_VtxWidth(width)
	, m_VtxHeight(height)
	, m_VtxDepth(depth)
	, m_TuMin(0.0f)
	, m_TuMax(1.0f)
	, m_TvMin(0.0f)
	, m_TvMax(1.0f)
{
	for(int i = 0; i < m_VtxNum; ++i)
	{
		m_Color[i] = 0xFFFFFFFF;
	}
}

Vertex::~Vertex()
{}

// UVスクロール関数
void Vertex::ScrollUV(float scrollSpeedTu, float scrollSpeedTv)
{
	m_TuMin += scrollSpeedTu;
	m_TuMax += scrollSpeedTu;
	m_TvMin += scrollSpeedTv;
	m_TvMax += scrollSpeedTv;
	CreateVtx();
}

// バーテックスサイズをセットする関数
void Vertex::SetSize(float vtxWidth, float vtxHeight, float vtxDepth)
{
	m_VtxWidth = vtxWidth;
	m_VtxHeight = vtxHeight;
	m_VtxDepth = vtxDepth;
	CreateVtx();
}

// tu,tv値を設定する関数
void Vertex::SetTuTvVal(float tuMin, float tuMax, float tvMin, float tvMax)
{
	m_TuMin = tuMin;
	m_TuMax = tuMax;
	m_TvMin = tvMin;
	m_TvMax = tvMax;
	CreateVtx();
}

// バーテックスの色情報を設定する関数
void Vertex::SetColor(DWORD color)
{
	for(int i = 0; i < m_VtxNum; ++i)
	{
		m_Color[i] = color;
	}
	CreateVtx();
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
