﻿//==================================================================================================================================//
//!< @file		InputKey.h
//!< @brief		InputKeyクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef INPUT_KEY_H
#define INPUT_KEY_H

#include "InputDevice.h"

/**キーに関するクラス*/
class InputKey
{

public:
	/**
	* Constructor
	* @param[in] pKeyDevice		キーボードデバイス
	*/
	explicit InputKey(LPDIRECTINPUTDEVICE8	pKeyDevice);

	/**Destructor*/
	~InputKey();

	/**キーの更新*/
	void UpdateKey();

	/**
	* キー状態確認関数
	* @param[in] DIK	ダイレクトインプットキー
	: @return キーの状態
	*/
	BTN_STATE CheckKey(int DIK);

private:
	static const int		m_KeyStateNum = 256;					//!< キーの種類
	LPDIRECTINPUTDEVICE8	m_pKeyDevice;							//!< キーボードデバイス格納用
	BYTE					m_DIKs[m_KeyStateNum];					//!< DIKを格納する変数
	BTN_STATE				m_CurrentKeyState[m_KeyStateNum];		//!< 現在のキーの状態を格納する変数
	BTN_STATE				m_OldKeyState[m_KeyStateNum];			//!< 前のキーの状態を格納する変数

	/**
	* 状態を確認する関数.
	* @param[in] DIK	ダイレクトインプットキー
	*/
	void CheckState(int DIK);

};

#endif		// INPUT_KEY_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
