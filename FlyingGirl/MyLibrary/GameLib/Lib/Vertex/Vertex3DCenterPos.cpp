﻿//==================================================================================================================================//
//!< @file		Vertex3DCenterPos.cpp
//!< @brief		Vertex3DCenterPosクラス実装
//!< @author	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "Vertex3DCenterPos.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions 
//--------------------------------------------------------------------------------------------------------------//

Vertex3DCenterPos::Vertex3DCenterPos(LPDIRECT3DDEVICE9 pD3Device, float width, float height, float depth)
	: Vertex(pD3Device, width, height, depth)
{
	CreateVtx();
}

Vertex3DCenterPos::~Vertex3DCenterPos()
{}

void Vertex3DCenterPos::Draw(LPDIRECT3DTEXTURE9 pTexture, float posX, float posY, float posZ)
{
	CUSTOM_VERTEX_3D drawVertex[m_VtxNum];

	for(char i = 0; i < m_VtxNum; ++i)
	{
		drawVertex[i]	   = m_Vtx[i];
		drawVertex[i].m_X += posX;
		drawVertex[i].m_Y += posY;
		drawVertex[i].m_Z += posZ;
	}

	m_pD3Device->SetTexture(0, pTexture);
	m_pD3Device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, drawVertex, sizeof(CUSTOM_VERTEX_3D));
}

//--------------------------------------------------------------------------------------------------------------//
//Private functions 
//--------------------------------------------------------------------------------------------------------------//

void Vertex3DCenterPos::CreateVtx()
{
	CUSTOM_VERTEX_3D vtex[] = {
		{ -m_VtxWidth / 2.0f,  m_VtxHeight / 2.0f, m_VtxDepth, m_Color[0], m_TuMin, m_TvMin },
		{  m_VtxWidth / 2.0f,  m_VtxHeight / 2.0f, m_VtxDepth, m_Color[1], m_TuMax, m_TvMin },
		{ -m_VtxWidth / 2.0f, -m_VtxHeight / 2.0f, m_VtxDepth, m_Color[2], m_TuMin, m_TvMax },
		{  m_VtxWidth / 2.0f, -m_VtxHeight / 2.0f, m_VtxDepth, m_Color[3], m_TuMax, m_TvMax },
	};

	for(char i = 0; i < m_VtxNum; ++i)
	{
		m_Vtx[i] = vtex[i];
	}
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
