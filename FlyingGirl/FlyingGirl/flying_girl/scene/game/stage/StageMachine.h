﻿//==================================================================================================================================//
//!< @file		StageMachine.h
//!< @brief		StageMachineクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef STAGE_MACHINE_H
#define STAGE_MACHINE_H

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace stage
{

class StageMachine
{

public:
	/** Constructor */
	StageMachine();

	/** Destructor */
	~StageMachine();
};

}	// namespace stage
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// STAGE_MACHINE_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
