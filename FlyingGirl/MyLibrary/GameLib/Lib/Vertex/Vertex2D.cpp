﻿//==================================================================================================================================//
//!< @file		Vertex2D.cpp
//!< @brief		Vertex2Dクラス実装
//!< @author	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "Vertex2D.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

Vertex2D::Vertex2D(LPDIRECT3DDEVICE9 pD3Device, float width, float height)
	: Vertex(pD3Device, width, height)
{
	CreateVtx();
}

Vertex2D::~Vertex2D()
{}

void Vertex2D::Draw(LPDIRECT3DTEXTURE9 pTexture, float posX, float posY, float posZ)
{
	CUSTOM_VERTEX_2D drawVertex[m_VtxNum];

	for(char i = 0; i < m_VtxNum; ++i)
	{
		drawVertex[i]	   = m_Vtx[i];
		drawVertex[i].m_X += posX;
		drawVertex[i].m_Y += posY;
	}

	m_pD3Device->SetTexture(0, pTexture);
	m_pD3Device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, drawVertex, sizeof(CUSTOM_VERTEX_2D));
}

//--------------------------------------------------------------------------------------------------------------//
//Private functions
//--------------------------------------------------------------------------------------------------------------//

void Vertex2D::CreateVtx()
{
	CUSTOM_VERTEX_2D vtex[] = {
		{		0.0f,		 0.0f, m_VtxDepth, 1.0f, m_Color[0], m_TuMin, m_TvMin },
		{ m_VtxWidth,		 0.0f, m_VtxDepth, 1.0f, m_Color[1], m_TuMax, m_TvMin },
		{		0.0f, m_VtxHeight, m_VtxDepth, 1.0f, m_Color[2], m_TuMin, m_TvMax },
		{ m_VtxWidth, m_VtxHeight, m_VtxDepth, 1.0f, m_Color[3], m_TuMax, m_TvMax },
	};

	for(char i = 0; i < m_VtxNum; ++i)
	{
		m_Vtx[i] = vtex[i];
	}
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
