﻿//==================================================================================================================================//
//!< @file		XFileManager.h
//!< @brief		XFileManagerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef XFILE_MANAGER_H
#define XFILE_MANAGER_H

#include <d3dx9.h>
#include <vector>

class XFile;

/**Xファイルを一括で管理するクラス*/
class XFileManager
{

public:
	/**
	* Constructor
	* @param[in] pD3Device	デバイスへのポインタ
	*/
	explicit XFileManager(LPDIRECT3DDEVICE9 pD3Device);

	/**Destructor*/
	~XFileManager();

	/**
	* Xファイルをロードする関数
	* @param[in]	pFilePath ファイル名
	* @return		登録した番号(キー)を返す
	*/
	int Load(const TCHAR* pFilePath);

	/**
	* Xファイルを描画する関数
	* @param[in] key 登録したキー
	*/
	void Draw(int key);

	/**
	* Xファイルを解放する関数
	* @param[in] key 登録したキー
	*/
	void Release(int key);

	/**マネージャーが管理しているすべてのファイルを解放する関数*/
	void ReleaseALL();

private:
	LPDIRECT3DDEVICE9		m_pD3Device;		//!< デバイスへのポインタ
	std::vector<XFile*>		m_pXFiles;			//!< XFileクラスのインスタンスへのポインタの動的配列

};

#endif	// XFILE_MANAGER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
