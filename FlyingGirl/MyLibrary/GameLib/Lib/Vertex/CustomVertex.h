﻿//==================================================================================================================================//
//!< @file		CustomVertex.h
//!< @brief		頂点情報構造体をまとめたヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef CUSTOM_VERETEX_H
#define	CUSTOM_VERETEX_H

/**頂点情報の基底構造体*/
struct BASIC_VERTEX
{
	FLOAT	m_X;		//!< 位置座標X
	FLOAT	m_Y;		//!< 位置座標Y
	FLOAT	m_Z;		//!< 位置座標Z

	BASIC_VERTEX(){}
	BASIC_VERTEX(FLOAT x, FLOAT y, FLOAT z)
		: m_X(x)
		, m_Y(y)
		, m_Z(z)
	{}

};


/**2Dで使用する頂点情報の構造体*/
struct CUSTOM_VERTEX_2D : public BASIC_VERTEX
{
	FLOAT	m_Rhw;			//!< 座標系
	DWORD	m_Color;		//!< 色
	FLOAT	m_Tu;			//!< テクスチャーのtu値
	FLOAT	m_Tv;			//!< テクスチャーのtv値

	CUSTOM_VERTEX_2D(){}
	CUSTOM_VERTEX_2D(FLOAT x, FLOAT y, FLOAT z, FLOAT rhw, DWORD color, FLOAT tu, FLOAT tv)
		: BASIC_VERTEX(x, y, z)
		, m_Rhw(rhw)
		, m_Color(color)
		, m_Tu(tu)
		, m_Tv(tv)
	{}

	void operator=(const CUSTOM_VERTEX_2D& rValues)
	{
		m_X		= rValues.m_X;
		m_Y		= rValues.m_Y;
		m_Z		= rValues.m_Z;
		m_Rhw	= rValues.m_Rhw;
		m_Color = rValues.m_Color;
		m_Tu	= rValues.m_Tu;
		m_Tv	= rValues.m_Tv;
	}

};

/**3Dで使用する頂点情報の構造体*/
struct CUSTOM_VERTEX_3D : public BASIC_VERTEX
{
	DWORD	m_Color;			//!< 色
	FLOAT	m_Tu;				//!< テクスチャーのtu値
	FLOAT	m_Tv;				//!< テクスチャーのtv値

	CUSTOM_VERTEX_3D(){}
	CUSTOM_VERTEX_3D(FLOAT x, FLOAT y, FLOAT z, DWORD color, FLOAT tu, FLOAT tv)
		: BASIC_VERTEX(x, y, z)
		, m_Color(color)
		, m_Tu(tu)
		, m_Tv(tv)
	{}

	void operator=(const CUSTOM_VERTEX_3D&  rValues)
	{
		m_X		= rValues.m_X;
		m_Y		= rValues.m_Y;
		m_Z		= rValues.m_Z;
		m_Color = rValues.m_Color;
		m_Tu	= rValues.m_Tu;
		m_Tv	= rValues.m_Tv;
	}
};

#endif	// CUSTOM_VERETEX_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
