﻿//==================================================================================================================================//
//!< @file		SceneManager.cpp
//!< @brief		SceneManagerクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "SceneManager.h"
#include "SceneFactory.h"
#include "../score/ScoreDataManager.h"
#include <windows.h>

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{

/* Public Functions ------------------------------------------------------------------------------------------- */

SceneManager::SceneManager()
	: m_pScene(nullptr)
	, m_pScoreDataManager(new score::ScoreDataManager())
	, m_CurrentStep(CREATE_SCENE)
	, m_CurrentSceneID(Scene::ID::LOGO)
	, m_NextSceneID(Scene::ID::LOGO)
	, m_IsGameEnd(false)
{
	SceneFactory::InitializeMember(m_pScoreDataManager);
}

SceneManager::~SceneManager()
{
	if(m_pScene != nullptr)
	{
		delete m_pScene;
		m_pScene = nullptr;
	}
	else
	{
		// テスト用処理. m_pScene == nullptrの場合の処理
	}

	delete m_pScoreDataManager;
	m_pScoreDataManager = nullptr;
}

bool SceneManager::Update()
{
	switch(m_CurrentStep)
	{
	case CREATE_SCENE:
		if(m_NextSceneID == Scene::ID::GAME_END)
		{
			m_IsGameEnd = true;
		}
		else
		{
			m_CurrentSceneID = m_NextSceneID;
			m_pScene = SceneFactory::CreateScene(m_CurrentSceneID);
			m_CurrentStep = UPDATE_SCENE;
		}
		break;

	case UPDATE_SCENE:
		if(m_pScene == nullptr)
		{
			MessageBox(0, "Sceneの作成に失敗しました。ゲームを強制終了します", "Error", MB_OK);
			m_IsGameEnd = true;
		}
		else
		{
			if((m_NextSceneID = m_pScene->Update()) != m_CurrentSceneID)
			{
				m_CurrentStep = DELETE_SCENE;
			}
			else
			{
				// テスト用処理. UPDATE_SCENEを継続. 
			}
		}
		break;

	case DELETE_SCENE:
		delete m_pScene;
		m_pScene = nullptr;
		m_CurrentStep = CREATE_SCENE;
		break;

	default:
		// do nothing
		break;
	}

	return m_IsGameEnd;
}

}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
