﻿//==================================================================================================================================//
//!< @file		GameLib.cpp
//!< @brief		GameLibクラス実装
//!< @author	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "Lib/Window/WindowCreator.h"	
#include "Lib/Graphics/GraphicsDevice.h"
#include "Lib/DirectInput/InputDevice.h"
#include "Lib/DirectInput/InputKey.h"
#include "Lib/DirectInput/InputMouse.h"
#include "Lib/Graphics/TextureManager.h"
#include "Lib/Graphics/XFileManager.h"
#include "Lib/Sound/SoundFileManager.h"
#include "Lib/Vertex/VertexManager.h"
#include "Lib/Animation/AnimationManager.h"
#include "Lib/DebugTool/DebugFont.h"
#include "Lib/DebugTool/DebugTimer.h"
#include "GameLib.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

// 初期化関数
HRESULT GameLib::InitGameLib(TCHAR*  title, int width, int height, LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM),
	bool isFullScreen, bool hasIcon, WORD iconID)
{
	m_wWidth = width;
	m_wHeight = height;
	HINSTANCE hInstance = GetModuleHandle(NULL);

	m_pWindowCreator = new WindowCreator(title, width, height);

	if(hasIcon)
	{
		m_pWindowCreator->CreateIcon(iconID);
	}

	m_pGraphicsDevice = new GraphicsDevice();

	if(FAILED(m_pWindowCreator->MakeWindow(hInstance, WndProc, isFullScreen)))
	{
		MessageBox(0, "Windowの初期化に失敗しました。", NULL, MB_OK);
		return E_FAIL;
	}

	if(FAILED(m_pGraphicsDevice->InitDevice(m_pWindowCreator->GetHwnd()
		, isFullScreen, m_wWidth, m_wHeight)))
	{
		MessageBox(0, "GraphicsDeviceの初期化に失敗しました。", NULL, MB_OK);
		return E_FAIL;
	}

	m_pGraphicsDevice->SetRenderState3D();

	m_pInputDevice = new InputDevice();
	m_pInputDevice->InitDinput();
	m_pInputDevice->InitDinputKey(m_pWindowCreator->GetHwnd());
	m_pInputDevice->InitDinputMouse(m_pWindowCreator->GetHwnd());
	m_pInputKey = new InputKey(m_pInputDevice->GetKeyDevice());
	m_pInputMouse = new InputMouse(m_pWindowCreator->GetHwnd(), m_pInputDevice->GetMouseDevice());

	m_pSoundFileManager = new SoundFileManager();
	m_pSoundFileManager->InitSound(m_pWindowCreator->GetHwnd());

	m_pTextureManager = new TextureManager(m_pGraphicsDevice->GetDevice());
	m_pXFileManager = new XFileManager(m_pGraphicsDevice->GetDevice());
	m_pVertexManager = new VertexManager(m_pGraphicsDevice->GetDevice());
	m_pAnimationManager = new AnimationManager();
	m_pDebugTimer = new DebugTimer(m_pGraphicsDevice->GetDevice());

	return S_OK;
}

// メモリ開放関数
void GameLib::ReleaseGameLib()
{
		delete m_pDebugTimer;
		m_pDebugTimer = NULL;

		delete m_pAnimationManager;
		m_pAnimationManager = NULL;

		delete m_pVertexManager;
		m_pVertexManager = NULL;

		delete m_pXFileManager;
		m_pXFileManager = NULL;

		delete m_pTextureManager;
		m_pTextureManager = NULL;

		delete m_pSoundFileManager;
		m_pSoundFileManager = NULL;

		delete m_pInputMouse;
		m_pInputMouse = NULL;

		delete m_pInputKey;
		m_pInputKey = NULL;

		delete m_pInputDevice;
		m_pInputDevice = NULL;

		delete m_pGraphicsDevice;
		m_pGraphicsDevice = NULL;

		delete m_pWindowCreator;
		m_pWindowCreator = NULL;
}

// ウィンドウタイプを変える関数
void GameLib::ChangeWindowMode()
{
	m_pGraphicsDevice->ChangeDisplayMode();
	m_pWindowCreator->ChangeWindowSize();
}

// ウィンドウの横幅を取得する関数
int GameLib::GetWindowWidth()
{
	return m_wWidth;
}

// ウィンドウの縦幅を取得する関数
int GameLib::GetWindowHeight()
{
	return m_wHeight;
}

// デバイスを取得する関数
IDirect3DDevice9* GameLib::GetDevice()
{
	return ( m_pGraphicsDevice->GetDevice() );
}

// 描画開始処理関数
void GameLib::StartRender(DWORD FVF)
{
	m_pGraphicsDevice->StartRender(FVF);
}

// 描画終了処理関数
void GameLib::EndRender()
{
	m_pGraphicsDevice->EndRender();
}

// 頂点フォーマットを設定する関数
void GameLib::SetFVF(DWORD FVF)
{
	m_pGraphicsDevice->SetFVF(FVF);
}

// テクスチャーを読み込む関数
int GameLib::LoadTex(TCHAR* pFilePath)
{
	 return m_pTextureManager->Load(pFilePath);
}

// テクスチャーを詳細設定して読み込む関数
int GameLib::LoadTexEx(TCHAR* pFilePath, int alpha, int red, int green, int blue, bool IsPowerTwo)
{
	return m_pTextureManager->LoadEx(pFilePath, alpha, red, green, blue, IsPowerTwo);
}

// テクスチャーのポインタを取得する関数
LPDIRECT3DTEXTURE9 GameLib::GetpTexture(int texKey)
{
	return m_pTextureManager->GetpTexture(texKey);
}

// 現在読み込んでいるテクスチャーを解放する関数
void GameLib::ReleaseTex(int texKey)
{
	m_pTextureManager->Release(texKey);
}

// 現在読み込んでいるテクスチャーの全て解放する関数
void GameLib::ReleaseTexALL()
{
	m_pTextureManager->ReleaseALL();
}

// 頂点情報(2D)を作成し、登録する関数
int GameLib::CreateVtx2D(float width, float height)
{
	return m_pVertexManager->CreateVtx2D(width, height);
}

// 位置座標が中心にある場合の頂点情報(2D)を作成し、登録する関数
int GameLib::CreateVtx2DCenterPos(float width, float height)
{
	return m_pVertexManager->CreateVtx2DCenterPos(width, height);
}

// 頂点情報(3Dのxy)を作成し、登録する関数
int GameLib::CreateVtx3D(float width, float height, float depth)
{
	return m_pVertexManager->CreateVtx3D(width, height, depth);
}

// 位置座標が中心にある場合の頂点情報(3Dのxy)を作成し、登録する関数
int GameLib::CreateVtx3DCeneterPos(float width, float height, float depth)
{
	return m_pVertexManager->CreateVtx3DCenterPos(width, height, depth);
}

// XZ平面のポリゴンの頂点情報(3D空間)を作成し、登録する関数
int GameLib::CreateVtx3Dxz(float width, float height, float depth)
{
	return m_pVertexManager->CreateVtx3Dxz(width, height, depth);
}

// 位置座標が中心にある場合のXZ平面のポリゴンの頂点情報(3D空間)を作成し、登録する関数
int GameLib::CreateVtx3DxzCeneterPos(float width, float height, float depth)
{
	return m_pVertexManager->CreateVtx3DxzCenterPos(width, height, depth);
}

// サイズを変更する関数
void GameLib::SetVtxSize(int key, float width, float height, float depth)
{
	m_pVertexManager->SetSize(key, width, height, depth);
}

// tu値とtv値を変更する関数
void GameLib::SetVtxTuTv(int key, float tuMin, float tuMax, float tvMin, float tvMax)
{
	m_pVertexManager->SetTuTv(key, tuMin, tuMax, tvMin, tvMax);
}

// 色を変更する関数
void GameLib::SetVtxColor(int key, DWORD color)
{
	m_pVertexManager->SetColor(key, color);
}

// UVスクロールを行う関数
void GameLib::ScrollUV(int key, float scrollSpeedTu, float scrollSpeedTv)
{
	m_pVertexManager->ScrollUV(key, scrollSpeedTu, scrollSpeedTv);
}

// 頂点情報の横幅を取得する関数
float GameLib::GetVtxWidth(int Key)
{
	return m_pVertexManager->GetWidth(Key);
}

// 頂点情報の縦幅を取得する関数
float GameLib::GetVtxHeight(int Key)
{
	return m_pVertexManager->GetHeight(Key);
}

// 頂点情報の奥行を取得する関数
float GameLib::GetVtxDepth(int Key)
{
	return m_pVertexManager->GetDepth(Key);
}

// 現在作成している頂点情報を解放する関数
void GameLib::ReleaseVtx(int key)
{
	m_pVertexManager->Release(key);
}

// 現在作成している頂点情報全てを開放する関数
void GameLib::ReleaseVtxALL()
{
	m_pVertexManager->ReleaseALL();
}

// アニメーション情報を登録する関数
void GameLib::CreateAnimation(int key, int tuCount, int tvCount, int interval)
{
	m_pAnimationManager->CreateAnimation(key, tuCount, tvCount, interval);
}

// アニメのUV情報をセットする関数
void GameLib::SetAnimeInfo(int key, int animeNum, float minTu, float maxTu, float minTv, float maxTv)
{
	m_pAnimationManager->SetAnimation(key, animeNum, minTu, maxTu, minTv, maxTv);
}

// アニメーションを更新する関数
bool GameLib::Update(int key, ANIME_MODE mode, int* pCurrentAnimeNum, int* pIntervalCount)
{
	bool result = false;

	switch(mode)
	{

	case ANIME_MODE::ASCENDING_NUM:
		result = m_pAnimationManager->Update(key, AnimationManager::ASCENDING_NUM, pCurrentAnimeNum, pIntervalCount);
		break;

	case ANIME_MODE::DESCENDING_NUM:
		result = m_pAnimationManager->Update(key, AnimationManager::DESCENDING_NUM, pCurrentAnimeNum, pIntervalCount);
		break;

	case ANIME_MODE::ASCENDING_NUM_ROOP:
		result = m_pAnimationManager->Update(key, AnimationManager::ASCENDING_NUM_ROOP, pCurrentAnimeNum, pIntervalCount);
		break;

	case ANIME_MODE::DESCENDING_NUM_ROOP:
		result = m_pAnimationManager->Update(key, AnimationManager::DESCENDING_NUM_ROOP, pCurrentAnimeNum, pIntervalCount);
		break;
	}

	return result;
}

// アニメーション情報を解放する関数
void GameLib::ReleaseAnimation(int key)
{
	m_pAnimationManager->Release(key);
}

// 登録しているアニメーション情報全てを解放する関数
void GameLib::ReleaseAnimationALL()
{
	m_pAnimationManager->ReleaseALL();
}

// 通常描画関数
void GameLib::Draw(int texKey, int vtxKey, float posX, float posY, float posZ)
{
	m_pVertexManager->Draw(vtxKey, m_pTextureManager->GetpTexture(texKey), posX, posY, posZ);
}

// アニメーションを行いながら描画する関数
void GameLib::DrawAnime(int texKey, int vtxKey, int animeKey, int animeNum, float posX, float posY, float posZ)
{
	const Animation::AnimeUV& rAnimeUV = m_pAnimationManager->GetAnimeUV(animeKey, animeNum);

	m_pVertexManager->SetTuTv(vtxKey
		, rAnimeUV.m_MinTu
		, rAnimeUV.m_MaxTu
		, rAnimeUV.m_MinTv
		, rAnimeUV.m_MaxTv
		);

	m_pVertexManager->Draw(vtxKey, m_pTextureManager->GetpTexture(texKey), posX, posY, posZ);
}
 
// Xファイルを読み込む関数
int GameLib::LoadXFile(const TCHAR* pFilePath)
{
	return m_pXFileManager->Load(pFilePath);
}

// Xファイルを描画する関数
void GameLib::DrawXFile(int key)
{
	m_pXFileManager->Draw(key);
}

// 登録しているXファイルを解放する関数.
void GameLib::ReleaseXFile(int key)
{
	m_pXFileManager->Release(key);
}

//  登録しているXファイル全てを解放する関数.
void GameLib::ReleaseXFileALL()
{
	m_pXFileManager->ReleaseALL();
}

// DirextInputをアップデートする関数
void GameLib::UpdateDI()
{
	m_pInputKey->UpdateKey();
	m_pInputMouse->UpdateMouse();
}

// キーの状態を確認する関数
DI_STATE GameLib::CheckKey(int DIK)
{
	DI_STATE state;

	switch(m_pInputKey->CheckKey(DIK))
	{
	case BTN_ON:
		state = ON;
		break;

	case BTN_OFF:
		state = OFF;
		break;

	case BTN_PUSH:
		state = PUSH;
		break;

	case BTN_RELEASE:
		state = RELEASE;
		break;
	}
	return state;
}

// マウスの左ボタンの状態を取得する関数
DI_STATE GameLib::ChecKMouseL()
{
	DI_STATE state;

	switch(m_pInputMouse->ChecKMouse(MouseLeft))
	{
	case BTN_ON:
		state = ON;
		break;

	case BTN_OFF:
		state = OFF;
		break;

	case BTN_PUSH:
		state = PUSH;
		break;

	case BTN_RELEASE:
		state = RELEASE;
		break;
	}
	return state;
}

// マウスの右ボタンの状態を取得する関数
DI_STATE GameLib::ChecKMouseR()
{
	DI_STATE state;

	switch(m_pInputMouse->ChecKMouse(MouseRight))
	{
	case BTN_ON:
		state = ON;
		break;

	case BTN_OFF:
		state = OFF;
		break;

	case BTN_PUSH:
		state = PUSH;
		break;

	case BTN_RELEASE:
		state = RELEASE;
		break;
	}
	return state;
}

// マウスのホイールの状態を取得する関数
WHEEL_STATE GameLib::GetWheelState()
{
	WHEEL_STATE state;

	switch(m_pInputMouse->GetWheelState())
	{
	case WHEEL_NONE:
		state = ROLL_NONE;
		break;

	case WHEEL_UP:
		state = ROLL_UP;
		break;

	case WHEEL_DOWN:
		state = ROLL_DOWN;
		break;
	}
	return state;
}

// マウスの座標を取得する関数
void GameLib::GetMousePos(float* mousePosX, float* mousePosY)
{
	*mousePosX = static_cast<float>( m_pInputMouse->GetPosX() );
	*mousePosY = static_cast<float>( m_pInputMouse->GetPosY() );
}

// マウスの座標をセットする関数
void GameLib::SetMouseCursorPos(float posX, float posY)
{
	m_pInputMouse->SetMouseCursorPos(posX, posY);
}

// マウスカーソルを表示するかどうかを設定する関数
void GameLib::ShowMouseCursor(bool isVisible)
{
	m_pInputMouse->ShowMouseCursor(isVisible);
}

// 音声を読み込む関数
int GameLib::LoadSound(TCHAR* pFilePath)
{
	return m_pSoundFileManager->LoadSound(pFilePath);
}

// 音楽を鳴らす関数
void GameLib::PlayDSound(int key, SOUND_OPERATION operation)
{
	switch(operation)
	{
	case SOUND_PLAY:
		m_pSoundFileManager->SoundPlayer(key, SOUND_MODE::PLAY);
		break;

	case SOUND_STOP:
		m_pSoundFileManager->SoundPlayer(key, SOUND_MODE::STOP);
		break;

	case SOUND_LOOP:
		m_pSoundFileManager->SoundPlayer(key, SOUND_MODE::PLAYLOOP);
		break;

	case SOUND_RESET:
		m_pSoundFileManager->SoundPlayer(key, SOUND_MODE::RESET);
		break;

	case SOUND_RESET_PLAY:
		m_pSoundFileManager->SoundPlayer(key, SOUND_MODE::RESET_PLAY);
		break;

	case SOUND_STOP_RESET:
		m_pSoundFileManager->SoundPlayer(key, SOUND_MODE::STOP_RESET);
		break;
	}
}

// 登録しているサウンドを解放する関数
void GameLib::ReleaseSound(int key)
{
	m_pSoundFileManager->Release(key);
}

// 登録しているサウンド全てを解放する関数
void GameLib::ReleaseSoundALL()
{
	m_pSoundFileManager->ReleaseALL();
}

// デバック用の文字を表示させる関数
void GameLib::DrawDebugFont(const std::string& rText, float posX, float posY)
{
	DebugFont font(m_pGraphicsDevice->GetDevice());
	font.Draw(rText.c_str(), D3DXVECTOR2(posX, posY));
}

// デバック用の時間計測を開始する関数
void GameLib::StartTimer(const std::string& rTimeName)
{
	m_pDebugTimer->Start(rTimeName);
}

// デバック用の時間計測を終了する関数
void GameLib::EndTimer(const std::string& rTimeName)
{
	m_pDebugTimer->End(rTimeName);
}

// デバック用の時間計測した結果を取得する関数
DWORD GameLib::GetMeasuringTime(const std::string& rTimeName)
{
	return (m_pDebugTimer->GetMeasuringResult(rTimeName));
}

// デバック用の計測結果を表示する関数
void GameLib::DrawMeasuringTime(const std::string& rTimeName, float posX, float posY)
{
	m_pDebugTimer->DrawMeasuringResult(rTimeName, D3DXVECTOR2(posX, posY));
}

// デバック用の時間計測した結果全てとその合計時間を表示する関数
void GameLib::DrawAllMeasuringTime(float posX, float posY)
{
	m_pDebugTimer->DrawAllResult(D3DXVECTOR2(posX, posY));
}

GameLib::~GameLib()
{
	ReleaseGameLib();
}

//-----------------------------------------------------------------------------------------------------//
//Private functions
//-----------------------------------------------------------------------------------------------------//

GameLib::GameLib()
	: m_pWindowCreator(NULL)
	, m_pGraphicsDevice(NULL)
	, m_pInputDevice(NULL)
	, m_pSoundFileManager(NULL)
	, m_pInputMouse(NULL)
	, m_pInputKey(NULL)
	, m_pTextureManager(NULL)
	, m_pXFileManager(NULL)
	, m_pVertexManager(NULL)
	, m_pAnimationManager(NULL)
	, m_wWidth(0)
	, m_wHeight(0)
{}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
