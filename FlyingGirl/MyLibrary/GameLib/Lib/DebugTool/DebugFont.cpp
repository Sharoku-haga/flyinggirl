﻿//==================================================================================================================================//
//!< @file		DebugFont.cpp
//!< @brief		DebugFontクラス実装
//!< @author	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "DebugFont.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

// 文字の属性は 高さ20、幅10、色は白で固定
DebugFont::DebugFont(LPDIRECT3DDEVICE9 pD3Device)
	: m_pD3Device(pD3Device)
	, m_pFont(NULL)
	, m_Height(20)
	, m_Width(10)
	, m_Color(D3DCOLOR_XRGB(255, 255, 255))
{
	if (FAILED(D3DXCreateFont(m_pD3Device,
		m_Height,
		m_Width,
		FW_REGULAR,
		NULL,
		FALSE,
		SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS,
		PROOF_QUALITY,
		FIXED_PITCH | FF_MODERN,
		TEXT("ＭＳ　Ｐゴシック"),
		&m_pFont)))
	{
		MessageBox(NULL, "フォントクラスを作成できません", "Error", MB_OK);
	}
}

DebugFont::~DebugFont()
{
	if (m_pFont != NULL)
	{
		Release();
	}
}

// 文字描画関数
void DebugFont::Draw(LPCTSTR pString, const D3DXVECTOR2& rPos)
{
	D3DXFONT_DESC desc;				// フォントの属性(構造体)
	m_pFont->GetDesc(&desc);

	RECT rect;						// 文字の表示範囲を格納
	rect.left = static_cast<LONG>(rPos.x);
	rect.top = static_cast<LONG>(rPos.y);
	rect.right = static_cast<LONG>(rPos.x) + desc.Width * strlen(pString);
	rect.bottom = static_cast<LONG>(rPos.y) + desc.Height * strlen(pString);
	m_pFont->DrawText(
		NULL,							// デバイスコンテキストのハンドル(NULLで良い)
		pString,						// 描画テキスト
		-1,								// 全て表示
		&rect,							// 表示範囲
		DT_LEFT,						// 左寄せ
		m_Color);						// 色
}

// メモリ開放関数
void DebugFont::Release()
{
	m_pFont->Release();
	m_pFont = NULL;
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
