﻿//==================================================================================================================================//
//!< @file		Player.h
//!< @brief		Playerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef PLAYER_H
#define PLAYER_H

#include "../ObjBase.h"

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace obj
{
namespace player
{

//======================================================================//
//!< プレイヤークラス
//======================================================================//
class Player : public ObjBase
{

public:
	/**
	* Constructor
	* @param[in] pos	位置座標
	* @param[in] texID	テクスチャーID
	*/
	Player(D3DXVECTOR2 pos, int texID);

	/** Destructor */
	virtual ~Player();

private:
	int			m_VtxID;		//!< VertexID

	/** 処理実行関数 */
	virtual void Run()override;

	/** 描画関数 */
	virtual void Render()override;

	/** 衝突判定処理を行う関数 */
	virtual void ExecuteCollisionProcessing()override;

};	// class Player

}	// namespace player
}	// namespace obj
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// PLAYER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
