﻿//==================================================================================================================================//
//!< @file		SceneFactory.h
//!< @brief		SceneFactoryクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef SCENE_FACTORY_H
#define SCENE_FACTORY_H

#include "Scene.h"

namespace flying_girl
{

namespace score
{

class ScoreDataManager;

}	// namespace score

namespace scene
{

//======================================================================//
//!< Sceneを生成するファクトリークラス
//!< モノステイトパターンの為、ConstructorとDestructorはprivate
//======================================================================//
class SceneFactory
{

public:
	/**
	* メンバー変数を初期化する関数.
	* @param[in] pScoreDataManager	score::DataManagerクラスのインスタンスへのポインタ
	*/
	static void		InitializeMember(score::ScoreDataManager* pScoreDataManager);

	/**
	* Sceneを生成する関数
	* @param[in] id	生成したいSceneのID
	*/
	static Scene*	CreateScene(Scene::ID id);

private:
	static score::ScoreDataManager*		m_pScoreDataManager;		//!< score::ScoreDataManagerクラスのインスタンスへのポインタ

	/** Constructor */
	SceneFactory() = default;

	/** Destructor */
	~SceneFactory() = default;

};	// class SceneFactory

}	// namespace scene
}	// namespace flying_girl

#endif	// SCENE_FACTORY_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
