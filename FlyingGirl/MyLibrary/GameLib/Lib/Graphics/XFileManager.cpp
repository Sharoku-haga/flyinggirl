﻿//==================================================================================================================================//
//!< @file		XFileManager.cpp
//!< @brief		XFileManagerクラス実装
//!< @author	haga
//==================================================================================================================================//

//---------------------------------------------------------------------------------------------------------------//
//Includes
//---------------------------------------------------------------------------------------------------------------//

#include "XFile.h"
#include "XFileManager.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

XFileManager::XFileManager(LPDIRECT3DDEVICE9 pD3Device)
	: m_pD3Device(pD3Device)
{}

XFileManager::~XFileManager()
{
	ReleaseALL();
}

// Xファイルを読み込む関数
int XFileManager::Load(const TCHAR* pFilePath)
{
	int key = m_pXFiles.size();

	m_pXFiles.emplace_back(new XFile(m_pD3Device));
	m_pXFiles[key]->LoadXFile(pFilePath);

	return key;
}

// Xファイルを描画する関数
void XFileManager::Draw(int key)
{
	m_pXFiles[key]->Draw();
}

// 解放関数
void XFileManager::Release(int key)
{
	m_pXFiles[key]->Release();
	delete m_pXFiles[key];
	m_pXFiles[key] = NULL;
}

// 管理しているファイルをすべて解放する関数
void XFileManager::ReleaseALL()
{
	for(auto& pFile : m_pXFiles)
	{
		if(pFile != NULL)
		{
			pFile->Release();
			delete pFile;
		}
	}
	std::vector<XFile*>().swap(m_pXFiles);
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
