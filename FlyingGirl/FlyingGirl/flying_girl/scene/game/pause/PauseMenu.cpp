﻿//==================================================================================================================================//
//!< @file		PauseMenu.cpp
//!< @brief		PauseMenuクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "PauseMenu.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace pause
{

/* Public Functions ------------------------------------------------------------------------------------------- */

PauseMenu::PauseMenu()
{}

PauseMenu::~PauseMenu()
{}

}	// namespace pause
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
