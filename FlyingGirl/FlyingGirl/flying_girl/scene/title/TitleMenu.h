﻿//==================================================================================================================================//
//!< @file		TitleMenu.h
//!< @brief		TitleMenuクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef TITLE_MENU_H
#define TITLE_MENU_H

#include <vector>
#include "TitleScene.h"

class GameLib;

namespace flying_girl
{

namespace btn
{

class Button;

}	// namespace btn 

namespace scene
{
namespace title
{

//======================================================================//
//!< タイトルの状態を選択し、管理するクラス
//======================================================================//
class TitleMenu
{

public:
	/** 
	* Constructor
	* @param[in] texID
	*/
	explicit TitleMenu(int texID);

	/** Destructor */
	~TitleMenu();

	/**
	* コントロール関数
	* @return TitleSceneの状態
	*/
	TitleScene::STATE Control();

	/** 描画関数 */
	void Draw();

private:
	/** TitleMenuにおけるボタンの種類 */
	enum BUTTON_TYPE
	{
		GAME_START_BTN,			//!< ゲーム開始ボタン
		SCORE_DISP_BTN,			//!< スコア表示ボタン
		GAME_END_BTN,			//!< ゲーム終了ボタン
		BUTTON_MAX
	};

	TitleScene::STATE			m_SelectedTitleState;				//!< 選んだTitleSceneの状態
	BUTTON_TYPE					m_CurrntSelectedBtn;				//!< 現在選択しているボタン
	std::vector<btn::Button*>	m_pButtons;							//!< btn::Buttonクラスのインスタンスへのポインタのvector

	GameLib&					m_rGameLib;							//!< GameLibクラス(Library)のインスタンスへの参照

};	// class TitleMenu

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

#endif	// TITLE_MENU_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
