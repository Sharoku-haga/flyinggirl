﻿//==================================================================================================================================//
//!< @file		ScaleFunction.cpp
//!< @brief		ScaleFunctionクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "ScaleFunction.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace btn
{
namespace func
{

/* Public Functions ------------------------------------------------------------------------------------------- */

ScaleFunction::ScaleFunction(Button* pButton, float scaleVal)
	: ButtonFunction(pButton)
	, m_ScaleVal(scaleVal)
	, m_CorrectionScaleVal(0.0f)
{}

ScaleFunction::~ScaleFunction()
{
	// pButtonのdeleteはButtonFunctionのデストラクタで行う
}

/* Private Functions ------------------------------------------------------------------------------------------ */

void ScaleFunction::Run()
{
	m_pButton->Control();
	m_CorrectionScaleVal = m_ScaleVal;
}

void ScaleFunction::Render()
{
	int	vtxID = m_pButton->GetVtxID();

	m_rGameLib.SetVtxSize(vtxID, (m_rGameLib.GetVtxWidth(vtxID) + m_CorrectionScaleVal), (m_rGameLib.GetVtxHeight(vtxID) + m_CorrectionScaleVal));

	m_pButton->Draw();

	// Vertexを元の状態に戻す
	m_rGameLib.SetVtxSize(vtxID, (m_rGameLib.GetVtxWidth(vtxID) - m_CorrectionScaleVal), (m_rGameLib.GetVtxHeight(vtxID) - m_CorrectionScaleVal));
	m_CorrectionScaleVal = 0;
}

}	// namespace func
}	// namespace btn
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
