﻿//==================================================================================================================================//
//!< @file		LogoScene.h
//!< @brief		LogoSceneクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef LOGO_SCENE_H
#define LOGO_SCENE_H

#include "../Scene.h"

class GameLib;

namespace flying_girl
{
namespace scene
{
namespace logo
{

class LogoBackground;

//======================================================================//
//!< ロゴを表示するクラス
//======================================================================//
class LogoScene : public Scene
{

public:
	/** Constructor */
	LogoScene();

	/** Destructor */
	virtual ~LogoScene();

private:
	LogoBackground*		m_pBackground;			//!< LogoBackgroundクラスのインスタンスへのポインタ

	/**
	* コントロール関数.
	* @return	次のSceneID
	*/
	virtual Scene::ID	Control()override;

	/** 描画関数 */
	virtual void		Draw()override;

};	// class LogoScene

}	// namespace logo
}	// namespace scene
}	// namespace flying_girl

#endif	// LOGO_SCENE_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
