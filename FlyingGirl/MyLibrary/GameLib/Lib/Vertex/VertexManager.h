﻿//==================================================================================================================================//
//!< @file		VertexManager.h
//!< @brief		VertexManagerクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef VERTEX_MANAGER_H
#define VERTEX_MANAGER_H

#include <d3dx9.h>
#include <vector>

class Vertex;

/** 頂点情報クラスを管理するクラス*/
class VertexManager
{

public:
	/**
	* Constructor
	* @param[in] pD3Device	デバイスへのポインタ
	*/
	explicit VertexManager(LPDIRECT3DDEVICE9 pD3Device);

	/**Destructor*/
	~VertexManager();

	/**
	* 頂点情報(2D空間)を作成し、登録する関数.
	* @param[in] width		横幅(X軸)
	* @param[in] height		高さ(Y軸)
	* @return 検索キー(添え字)	
	*/
	int CreateVtx2D(float width, float height);

	/**
	* 位置座標が中心の頂点情報(2D空間)を作成し、登録する関数.
	* @param[in] width		横幅(X軸)
	* @param[in] height		高さ(Y軸)
	* @return 検索キー(添え字)
	*/
	int CreateVtx2DCenterPos(float width, float height);

	/**
	* 頂点情報(3D空間)を作成し、登録する関数.
	* @param[in] width		横幅(X軸)
	* @param[in] height		高さ(Y軸)
	* @param[in] depth		奥行き(Z軸)
	* @return				検索キー(添え字)	
	*/
	int CreateVtx3D(float width, float height, float depth);

	/**
	* 位置座標が中心の頂点情報(3D空間)を作成し、登録する関数.
	* @param[in] width		横幅(X軸)
	* @param[in] height		高さ(Y軸)
	* @param[in] depth		奥行き(Z軸)
	* @return				検索キー(添え字)
	*/
	int CreateVtx3DCenterPos(float width, float height, float depth);


	/**
	* XZ平面の頂点情報(3D空間)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth	奥行き(Z軸)
	* @return			検索キー(添え字)
	*/
	int CreateVtx3Dxz(float width, float height, float depth);

	/**
	* 位置座標が中心のXZ平面の頂点情報(3D空間)を作成し、登録する関数.
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth	奥行き(Z軸)
	* @return			検索キー(添え字)
	*/
	int CreateVtx3DxzCenterPos(float width, float height, float depth);


	/**
	* 通常描画関数.
	* @param[in] key		登録キー(添え字)
	* @param[in] pTexture	テクスチャーへのポインタ
	* @param[in] posX		X軸の位置座標(デフォルト引数0.0f)
	* @param[in] posY		Y軸の位置座標(デフォルト引数0.0f)
	* @param[in] posZ		Z軸の位置座標(デフォルト引数0.0f)
	*/
	void Draw(int key, LPDIRECT3DTEXTURE9 pTexture, float posX = 0.0f, float posY = 0.0f, float posZ = 0.0f);

	/**
	* サイズを変更する関数
	* @param[in] key	登録キー(添え字)
	* @param[in] width	横幅(X軸)
	* @param[in] height 高さ(Y軸)
	* @param[in] depth  奥行(Z軸)
	*/
	void SetSize(int key, float width, float height, float depth = 0.0f);

	/**
	* tu値とtv値を変更する関数
	* @param[in] key	登録キー(添え字)
	* @param[in] tuMin  tu値の最小値
	* @param[in] tuMax  tu値の最大値
	* @param[in] tvMin  tv値の最小値
	* @param[in] tvax	tv値の最大値
	*/
	void SetTuTv(int key, float tuMin, float tuMax, float tvMin, float tvMax);

	/**
	* 色を変更する関数
	* @param[in] key	登録キー(添え字)
	* @param[in] color  色
	*/
	void SetColor(int key, DWORD color);

	/**
	* UVスクロールを行う関数
	* @param[in] key			登録キー(添え字)
	* @param[in] scrollSpeedTu	tuのスクロールの速さ(変化量)
	* @param[in] scrollSpeedTv	tvのスクロールの速さ(変化量)
	*/
	void ScrollUV(int key, float scrollSpeedTu, float scrollSpeedTv);
	
	/**
	* 横幅を取得する関数
	* @param key	登録キー(添え字)
	* @return バーテックスの横幅
	*/
	float GetWidth(int key);

	/**
	* 縦幅を取得する関数
	* @param key	登録キー(添え字)
	* @return バーテックスの縦幅
	*/
	float GetHeight(int key);

	/**
	* 奥行を取得する関数
	* @param key	登録キー(添え字)
	* @return バーテックスの奥行
	*/
	float GetDepth(int key);

	/**
	* 頂点情報を解放する関数
	* @param[in] key	登録キー(添え字)
	*/
	void Release(int key);

	/**全ての頂点情報を解放する関数*/
	void ReleaseALL();

	/**
	* 現在の作成している頂点情報の数を取得する関数.
	* デバック用として作成
	*/
	inline int GetVertexNum() { return m_pVertex.size(); }

private:
	LPDIRECT3DDEVICE9					m_pD3Device;		//!< デバイスへのポインタ
	std::vector<Vertex*>				m_pVertex;			//!< 頂点情報を格納する変数  

};

#endif	// VERTEX_MANAGER_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
