﻿//==================================================================================================================================//
//!< @file		ObjBase.cpp
//!< @brief		ObjBaseクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "ObjBase.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace obj
{

/* Public Functions ------------------------------------------------------------------------------------------- */

ObjBase::ObjBase(D3DXVECTOR2 pos, int texID)
	: m_Pos(pos)
	, m_TexID(texID)
	, m_HasVanished(false)
	, m_rGameLib(GameLib::Instance())
{}

bool ObjBase::Control()
{
	if(m_HasVanished)
	{	// 消失している場合
		return true;
	}
	else
	{
		// テスト用空処理 消失していない(存在している)場合
	}

	Run();

	return false;
}

void  ObjBase::Draw()
{
	Render();
}

void ObjBase::CheckCollision()
{
	ExecuteCollisionProcessing();
}

}	// namespace obj
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
