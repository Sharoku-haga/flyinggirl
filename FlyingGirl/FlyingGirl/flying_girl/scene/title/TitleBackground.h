﻿//==================================================================================================================================//
//!< @file		TitleBackground.h
//!< @brief		TitleBackgroundクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef TITLE_BACKGROUND_H
#define TITLE_BACKGROUND_H

#include <d3dx9.h>

class GameLib;

namespace flying_girl
{
namespace scene
{
namespace title
{

//======================================================================//
//!< TitleSceneの背景を描画するクラス
//======================================================================//
class TitleBackground
{

public:
	/** 
	* Constructor
	* @param[in] texID TextureID
	*/
	explicit TitleBackground(int texID);

	/** Desutructor */
	~TitleBackground();

	/** 描画関数 */
	void Draw();

private:
	D3DXVECTOR2			m_Pos;			//!< 位置座標
	int					m_TexID;		//!< TextureID
	int					m_VtxID;		//!< VertexID

	GameLib&			m_rGameLib;		//!< GameLibクラス(Library)のインスタンスへの参照

};	// class TitleBackground

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

#endif	// TITLE_BACKGROUND_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
