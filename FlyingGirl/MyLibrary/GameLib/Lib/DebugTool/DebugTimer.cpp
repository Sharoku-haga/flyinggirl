﻿//==================================================================================================================================//
//!< @file		DebugTimer.cpp
//!< @brief		DebugTimerクラス実装
//!< @author	haga
//==================================================================================================================================//

//---------------------------------------------------------------------------------------------------------------//
//Includes
//---------------------------------------------------------------------------------------------------------------//

#include <windows.h>
#include "DebugFont.h"
#include "DebugTimer.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

DebugTimer::DebugTimer(LPDIRECT3DDEVICE9 pD3Device)
	: m_pD3Device(pD3Device)
{}

DebugTimer::~DebugTimer()
{
	m_DebugTime.clear();
}

// 時間計測開始
void DebugTimer::Start(const std::string&  rTimeName)
{
	auto& itr = m_DebugTime.find(rTimeName);
	if(itr == m_DebugTime.end())
	{
		m_DebugTime[rTimeName] = timeGetTime();
	}
	else
	{
		MessageBox(NULL, "この計測時間名はすでに使用されています", rTimeName.c_str(), MB_OK);
	}
}

// 時間計測終了
void DebugTimer::End(const std::string&  rTimeName)
{
	auto& itr = m_DebugTime.find(rTimeName);
	if(itr != m_DebugTime.end())
	{
		DWORD currentTime = timeGetTime();
		m_DebugTime[rTimeName] = currentTime - m_DebugTime[rTimeName];
	}
	else
	{
		MessageBox(NULL, "この計測時間は設定されていません", rTimeName.c_str(), MB_OK);
	}
}

// 計測結果の取得
DWORD DebugTimer::GetMeasuringResult(const std::string&  rTimeName)
{
	auto& itr = m_DebugTime.find(rTimeName);
	if(itr != m_DebugTime.end())
	{
		return m_DebugTime[rTimeName];
	}

	MessageBox(NULL, "この計測時間は設定されていないので結果を取得できません", rTimeName.c_str(), MB_OK);
	
	return 0;
}

// 計測結果表示
void DebugTimer::DrawMeasuringResult(const std::string&  rTimeName, const D3DXVECTOR2& rPos)
{
	auto& itr = m_DebugTime.find(rTimeName);
	if(itr != m_DebugTime.end())
	{
		std::string  Str =
			rTimeName + ":" + std::to_string(m_DebugTime[rTimeName]) + "ミリ秒" + "\n";
		DebugFont timeDisp(m_pD3Device);
		timeDisp.Draw(Str.c_str(), rPos);
	}
	else
	{
		MessageBox(NULL, "この計測時間は設定されていません", rTimeName.c_str(), MB_OK);
	}
}

// 時間の総合計を表示する
void DebugTimer::DrawTotalResult(const D3DXVECTOR2& rPos)
{
	if(m_DebugTime.size() <= 1)			
	{
		// 計測結果が1つ、もしくは1つも設定されていない場合はメッセージを表示
		MessageBox(NULL, "計測時間が2つ以上設定されていません", "Error", MB_OK);
		return;
	}

	DWORD totalTime = 0;				// 合計結果を表示するための器

	for(auto& time :m_DebugTime)
	{
		totalTime += time.second;
	}

	std::string  Str =
		"合計時間は:" +  std::to_string(totalTime) + "ミリ秒" + "\n";
	DebugFont timeDisp(m_pD3Device);
	timeDisp.Draw(Str.c_str(), rPos);
}

// すべての計測時間と合計時間を表示する
void DebugTimer::DrawAllResult(const D3DXVECTOR2 rPos)
{
	if(m_DebugTime.empty())
	{
		MessageBox(NULL, "計測時間が1つも設定されていません", "Error", MB_OK);
		return;
	}

	D3DXVECTOR2 dispPos = rPos;					// 表示用座標
	DebugFont timeDisp(m_pD3Device);							// 表示用フォント
	int fontHeight = timeDisp.GetFontHeight();	// 1文字の高さ
	int dispCount = 0;							// 表示数

	for(auto& time : m_DebugTime)
	{
		dispPos.y += (fontHeight * dispCount);				// 文字の高さ下に座標をずらす
		std::string  Str =
			time.first + ":" + std::to_string(time.second) + "ミリ秒" + "\n";
		timeDisp.Draw(Str.c_str(), dispPos);
		++dispCount;
	}

	// 計測結果が2つ以上ならば合計も表示する
	if (dispCount > 1)
	{
		dispPos.y += fontHeight;
		DrawTotalResult(dispPos);
	}
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
