﻿//==================================================================================================================================//
//!< @file		BrightnessFunction.h
//!< @brief		BrightnessFunctionクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef BRIGHTNESS_FUNCTION_H
#define BRIGHTNESS_FUNCTION_H

#include <d3dx9.h>
#include "ButtonFunction.h"

namespace flying_girl
{
namespace btn
{
namespace func
{

//======================================================================//
//!< ボタンの明暗を調節する機能のクラス
//!< @attention BasicButtonクラスと一緒に必ず使用すること
//======================================================================//
class BrightnessFunction : public ButtonFunction
{

public:
	/**
	* Constructor
	* @param[in] pButton		Buttonクラスのインスタンスへのポインタ
	* @param[in] brightnessVal  明暗処理のデフォルト値(デフォルト引数 = 0xffa3a3a3)
	*/
	BrightnessFunction(Button* pButton, DWORD brightnessVal = 0xffa3a3a3);

	/**Destructor*/
	virtual ~BrightnessFunction();

private:
	const DWORD		m_BrightnessVal;					//!< 明暗処理のデフォルト値 
	DWORD			m_CurrntBrightnessVal;				//!< 現在の明暗処理する値
	
	/** 処理実行関数 */
	virtual void	Run()override;

	/** 描画処理関数 */
	virtual void	Render()override;

};	// class BrightnessFunction 

}	// namespace func
}	// namespace btn
}	// namespace flying_girl

#endif	// BRIGHTNESS_FUNCTION_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
