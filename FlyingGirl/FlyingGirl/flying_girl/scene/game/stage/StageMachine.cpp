﻿//==================================================================================================================================//
//!< @file		StageMachine.cpp
//!< @brief		StageMachineクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "StageMachine.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace stage
{

/* Public Functions ------------------------------------------------------------------------------------------- */

StageMachine::StageMachine()
{}

StageMachine::~StageMachine()
{}

}	// namespace stage
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
