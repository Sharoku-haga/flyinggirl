﻿//==================================================================================================================================//
//!< @file		Button.h
//!< @brief		Buttonクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef BUTTON_H
#define BUTTON_H

class GameLib;

namespace flying_girl
{
namespace btn
{

//======================================================================//
//!< Buttonの基底クラス
//======================================================================//
class Button
{

public:
	/** Constructor */
	Button();

	/** Destructor */
	virtual ~Button() = default;

	/** コントロール関数 */
	void Control();

	/** 描画関数 */
	void Draw();

	/**
	* VetexIDを取得する関数
	* 公開関数
	* @return VertexID
	*/
	int	GetVtxID(){ return GetVertexID(); }

protected:
	GameLib&		m_rGameLib;			//!< GameLib(Library)のインスタンスへの参照

	/** 処理実行関数 */
	virtual void	Run() = 0;

	/** 描画処理関数 */
	virtual void	Render() = 0;

	/**
	* VetexIDを取得する関数
	* 非公開仮想関数
	* @return VertexID
	*/
	virtual int		GetVertexID() = 0;

};	// class Button


}	// namespace btn
}	// namespace flying_girl

#endif	// BUTTON_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
