﻿//==================================================================================================================================//
//!< @file		Item.h
//!< @brief		Itemクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef ITEM_H
#define ITEM_H

#include "../ObjBase.h"

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace obj
{
namespace item
{

//======================================================================//
//!< アイテムの基底クラス
//======================================================================//
class Item : public ObjBase
{

public:
	/**
	* Constructor
	* @param[in] pos	位置座標
	* @param[in] texID	テクスチャーID
	*/
	Item(D3DXVECTOR2 pos, int texID);

	/** Destructor */
	virtual ~Item() = default;

protected:
	/** 処理実行関数 */
	virtual void Run() = 0;

	/** 描画関数 */
	virtual void Render() = 0;

	/** 衝突判定処理を行う関数 */
	virtual void ExecuteCollisionProcessing() = 0;

};	// class Item

}	// namespace item
}	// namespace obj		
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// ITEM_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
