﻿//==================================================================================================================================//
//!< @file		Bullet.h
//!< @brief		Bulletクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef BULLET_H
#define BULLET_H

#include "../ObjBase.h"

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace obj
{
namespace bullet
{

class Bullet : public ObjBase
{

public:
	/**
	* Constructor
	* @param[in] pos	位置座標
	* @param[in] texID	テクスチャーID
	*/
	Bullet(D3DXVECTOR2 pos, int texID);

	/** Destructor */
	virtual ~Bullet() = default;

protected:
	/** 処理実行関数 */
	virtual void Run() = 0;

	/** 描画関数 */
	virtual void Render() = 0;

	/** 衝突判定処理を行う関数 */
	virtual void ExecuteCollisionProcessing() = 0;


};	// class Bullet

}	// namespace bullet
}	// namespace obj		
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// BULLET_H

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
