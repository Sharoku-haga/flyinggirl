﻿//==================================================================================================================================//
//!< @file		ButtonFactory.h
//!< @brief		ButtonFactoryクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef BUTTON_FACTORY_H
#define BUTTON_FACTORY_H

#include <d3dx9.h>
#include "Button.h"
#include "BasicButton.h"

namespace flying_girl
{
namespace btn
{

//======================================================================//
//!< ボタンの機能を生成し、ボタン本体に追加するクラス
//!< モノステイトパターン
//!< @todo 現在は機能ごとに関数を作成しているが、1つにまとめたい
//======================================================================//
class ButtonFactory
{

public:
	/**
	* ボタンのサイズ変更を行う機能を追加する関数
	* @param[in] pButton	機能を追加したいButtonクラスのインスタンスへのポインタ
	* @param[in] scaleVal	スケール処理の変化量(デフォルト引数 = 10.f)
	*/
	static	Button*	AddScaleFunction(Button* pButton, float scaleVal = 10.f);

	/**
	* Constructor
	* @param[in] pButton		Buttonクラスのインスタンスへのポインタ
	* @param[in] brightnessVal  明暗処理のデフォルト値(デフォルト引数 = 0xffa3a3a3)
	*/
	static	Button*	AddBrightnessFunction(Button* pButton, DWORD brightnessVal = 0xffa3a3a3);

private:

	/** Constructor */
	ButtonFactory() = default;

	/** Destructor*/
	~ButtonFactory() = default;

};	// class ButtonFactory

}	// namespace btn
}	// namespace flying_girl

#endif	// BUTTON_FACTORY_H


//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
