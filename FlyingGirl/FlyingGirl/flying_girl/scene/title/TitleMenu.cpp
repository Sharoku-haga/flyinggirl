﻿//==================================================================================================================================//
//!< @file		TitleMenu.cpp
//!< @brief		TitleMenuクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include <d3dx9.h>
#include "TitleMenu.h"
#include "GameLib/GameLib.h"
#include "../../btn/ButtonFactory.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace title
{

/* Public Functions ------------------------------------------------------------------------------------------- */

/** @todo 2017/02/24 数値は全部仮のもの */
TitleMenu::TitleMenu(int texID)
	: m_SelectedTitleState(TitleScene::GAME_START)
	, m_CurrntSelectedBtn(GAME_START_BTN)
	, m_rGameLib(GameLib::Instance())
{
	// ボタンの情報
	D3DXVECTOR2	startPos		= {640.f, 630.f};
	float		scrollPosYVal	= 100.f;
	float		btnWidth		= 500.f;
	float		btnHeight		= 100.f;
	float		btnTu			= 1.0f;
	float		btnTv			= 0.33f;

	for(int createBtnCount = 0; createBtnCount < BUTTON_MAX; ++createBtnCount)
	{
		// VertexIDを取得し、そのIDでボタンのtuとtv値を設定する
		int vtxID = m_rGameLib.CreateVtx2DCenterPos(btnWidth, btnHeight);
		m_rGameLib.SetVtxTuTv(vtxID, 0.0f, btnTu, (0.0f + btnTv * createBtnCount), (btnTv + btnTv * createBtnCount));

		// y座標はボタンを作成するたびに100.ｆごとずらしていく
		D3DXVECTOR2	pos = {startPos.x, (startPos.y + (scrollPosYVal * createBtnCount))};
		btn::Button* pButton = nullptr;
		pButton = btn::ButtonFactory::AddScaleFunction(btn::ButtonFactory::AddBrightnessFunction(new btn::BasicButton(pos, texID, vtxID, btnWidth, btnHeight)));
		m_pButtons.push_back(pButton);
	}
}

TitleMenu::~TitleMenu()
{
	for(auto& pButton : m_pButtons)
	{
		m_rGameLib.ReleaseVtx(pButton->GetVtxID());
	}

	for(auto& pButton : m_pButtons)
	{
		delete pButton;
		pButton = nullptr;
	}
	// Textureの解放はリTextureを読み込んだ場所で行う
}

TitleScene::STATE TitleMenu::Control()
{
	switch(m_CurrntSelectedBtn)
	{
	case GAME_START_BTN:
		if(m_rGameLib.CheckKey(DIK_DOWN) == PUSH)
		{
			m_CurrntSelectedBtn		= SCORE_DISP_BTN;
			m_SelectedTitleState	= TitleScene::DISPLAY_SCORE;
		}
		else
		{
			m_pButtons[GAME_START_BTN]->Control();
		}
		break;

	case SCORE_DISP_BTN:
		if(m_rGameLib.CheckKey(DIK_UP) == PUSH)
		{
			m_CurrntSelectedBtn		= GAME_START_BTN;
			m_SelectedTitleState	= TitleScene::GAME_START;
		}
		else if(m_rGameLib.CheckKey(DIK_DOWN) == PUSH)
		{
			m_CurrntSelectedBtn		= GAME_END_BTN;
			m_SelectedTitleState	= TitleScene::GAME_END;
		}
		else
		{
			m_pButtons[SCORE_DISP_BTN]->Control();
		}
		break;

	case GAME_END_BTN:
		if(m_rGameLib.CheckKey(DIK_UP) == PUSH)
		{
			m_CurrntSelectedBtn		= SCORE_DISP_BTN;
			m_SelectedTitleState	= TitleScene::DISPLAY_SCORE;
		}
		else
		{
			m_pButtons[GAME_END_BTN]->Control();
		}
		break;

	default:
		// do nothing
		break;
	}

	if(m_rGameLib.CheckKey(DIK_Z) == PUSH)
	{
		return m_SelectedTitleState;
	}
	else
	{
		// テスト用空処理. TitleSceneのStateがTitleScene::SELECT_MENUのまま
	}

	return TitleScene::SELECT_MENU;
}

void TitleMenu::Draw()
{
	for(auto& pButton : m_pButtons)
	{
		pButton->Draw();
	}
}

}	// namespace title
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
