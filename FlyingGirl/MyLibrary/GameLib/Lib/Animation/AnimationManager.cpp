﻿//==================================================================================================================================//
//!< @file		AnimationManager.cpp
//!< @brief		AnimationManagerクラス実装
//!< @author	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "AnimationManager.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

AnimationManager::AnimationManager()
{}

AnimationManager::~AnimationManager()
{
	ReleaseALL();
}

// Animationクラスを生成する関数
void AnimationManager::CreateAnimation(int key, int tuCount, int tvCount, int interval)
{
	m_pAnimation[key] = new Animation(tuCount, tvCount, interval);
}

// アニメーション情報を更新する関数
bool AnimationManager::Update(int key, MODE mode, int* pCurrentAnimeNum, int* pIntervalCount)
{
	switch(mode)
	{

	case ASCENDING_NUM:
		if(*pCurrentAnimeNum < m_pAnimation[key]->GetAnimePattern())
		{
			(*pCurrentAnimeNum) += Control(key, pIntervalCount);
		}
		else
		{
			return true;
		}
		break;

	case DESCENDING_NUM:
		if(*pCurrentAnimeNum > 0)
		{
			(*pCurrentAnimeNum) -= Control(key, pIntervalCount);
		}
		else
		{
			return true;
		}
		break;

	case ASCENDING_NUM_ROOP:
		if(*pCurrentAnimeNum < m_pAnimation[key]->GetAnimePattern())
		{
			(*pCurrentAnimeNum) += Control(key, pIntervalCount);
		}
		else
		{
			(*pCurrentAnimeNum) = 0;
		}
		break;

	case DESCENDING_NUM_ROOP:
		if(*pCurrentAnimeNum > 0)
		{
			(*pCurrentAnimeNum) -= Control(key, pIntervalCount);
		}
		else
		{
			(*pCurrentAnimeNum) = (m_pAnimation[key]->GetAnimePattern() - 1);
		}
		break;
	}

	return false;
}

//  Animationを更新する関数
void AnimationManager::SetAnimation(int key, int animeNum, float minTu, float maxTu, float minTv, float maxTv)
{
	m_pAnimation[key]->SetAnimeInfo(animeNum, minTu, maxTu, minTv, maxTv);
}

// スタートするアニメーションの番号を取得する関数
int AnimationManager::GetStartAnimeNum(int key, MODE mode)
{
	int animeNum = 0;

	switch(mode)
	{

	case ASCENDING_NUM:
		break;

	case DESCENDING_NUM:
		animeNum = (m_pAnimation[key]->GetAnimePattern() - 1);
		break;

	case ASCENDING_NUM_ROOP:
		break;

	case DESCENDING_NUM_ROOP:
		animeNum = (m_pAnimation[key]->GetAnimePattern() - 1);
		break;
	}

	return animeNum;
}

// アニメーションを解放する関数
void AnimationManager::Release(int key)
{
	delete m_pAnimation[key];
	m_pAnimation.erase(key);
}

// 全てのアニメーション情報を解放する関数
void AnimationManager::ReleaseALL()
{
	for(auto& anime : m_pAnimation)
	{
		delete anime.second;
	}

	m_pAnimation.clear();
}

//--------------------------------------------------------------------------------------------------------------//
//Private functions
//--------------------------------------------------------------------------------------------------------------//

int AnimationManager::Control(int key, int* pIntervalCount)
{
	int updateNum = 0;

	if(*pIntervalCount == m_pAnimation[key]->GetInterval())
	{
		updateNum = 1;
		(*pIntervalCount) = 0;
	}
	else
	{
		++(*pIntervalCount);
	}
	
	return updateNum;
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
