﻿//==================================================================================================================================//
//!< @file		Vertex3Dxz.cpp
//!< @brief		Vertex3Dxzクラス実装
//!< @auhtor	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "Vertex3Dxz.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

Vertex3Dxz::Vertex3Dxz(LPDIRECT3DDEVICE9 pD3Device, float width, float height, float depth)
	: Vertex(pD3Device, width, height, depth)
{
	CreateVtx();
}

Vertex3Dxz::~Vertex3Dxz()
{}

void Vertex3Dxz::Draw(LPDIRECT3DTEXTURE9 pTexture, float posX, float posY, float posZ)
{
	CUSTOM_VERTEX_3D drawVertex[m_VtxNum];

	for(char i = 0; i < m_VtxNum; ++i)
	{
		drawVertex[i]	   = m_Vtx[i];
		drawVertex[i].m_X += posX;
		drawVertex[i].m_Y += posY;
		drawVertex[i].m_Z += posZ;
	}

	m_pD3Device->SetTexture(0, pTexture);
	m_pD3Device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, drawVertex, sizeof(CUSTOM_VERTEX_3D));
}

//--------------------------------------------------------------------------------------------------------------//
//Private functions
//--------------------------------------------------------------------------------------------------------------//

void Vertex3Dxz::CreateVtx()
{
	CUSTOM_VERTEX_3D vtex[] =
	{
		{		0.0f, m_VtxHeight, m_VtxDepth, m_Color[0], m_TuMin, m_TvMin },
		{ m_VtxWidth, m_VtxHeight, m_VtxDepth, m_Color[1], m_TuMax, m_TvMin },
		{		0.0f, m_VtxHeight,		 0.0f, m_Color[2], m_TuMin, m_TvMax },
		{ m_VtxWidth, m_VtxHeight,		 0.0f, m_Color[3], m_TuMax, m_TvMax },
	};

	for(char i = 0; i < m_VtxNum; ++i)
	{
		m_Vtx[i] = vtex[i];
	}
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
