﻿//==================================================================================================================================//
//!< @file		TextureManager.cpp
//!< @brief		TextureManagerクラス実装
//!< @author	haga
//==================================================================================================================================//

//--------------------------------------------------------------------------------------------------------------//
//Includes
//--------------------------------------------------------------------------------------------------------------//

#include "TextureManager.h"

//--------------------------------------------------------------------------------------------------------------//
//Public functions
//--------------------------------------------------------------------------------------------------------------//

TextureManager::TextureManager(LPDIRECT3DDEVICE9 pD3Device)
	: m_pD3Device(pD3Device)
{}

TextureManager::~TextureManager()
{
	ReleaseALL();
}

int TextureManager::Load(TCHAR* pFilePath)
{
	int key = m_pTextures.size();

	LPDIRECT3DTEXTURE9 pTexture = NULL;
	m_pTextures.push_back(pTexture);

	if (FAILED(D3DXCreateTextureFromFile(m_pD3Device, pFilePath, &m_pTextures[key])))
	{
		// 読み込み失敗したらエラー
		MessageBox(0, "画像の読み込みに失敗しました。", NULL, MB_OK);
	}
	return key;
}

int TextureManager::LoadEx(TCHAR* pFilePath, int alpha, int red, int green, int blue, bool IsPowerTwo)
{
	int key = m_pTextures.size();
	LPDIRECT3DTEXTURE9 pTexturre = NULL;
	m_pTextures.push_back(pTexturre);

	if(IsPowerTwo)
	{
		if (FAILED(D3DXCreateTextureFromFileEx(
			m_pD3Device,			//	インターフェイスへのポインタ
			pFilePath,					//	画像ファイル名
			D3DX_DEFAULT,
			D3DX_DEFAULT,
			D3DX_DEFAULT,
			0,
			D3DFMT_UNKNOWN,
			D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE,
			D3DX_DEFAULT,
			D3DCOLOR_ARGB(alpha, red, green, blue),
			NULL,
			NULL,
			&m_pTextures[key]
			)))
		{
			MessageBox(0, "画像の読み込みに失敗しました。", NULL, MB_OK);
		}
	}
	else			//２のべき乗じゃないのなら
	{
		if (FAILED(D3DXCreateTextureFromFileEx(
			m_pD3Device,
			pFilePath,
			D3DX_DEFAULT_NONPOW2,
			D3DX_DEFAULT_NONPOW2,
			D3DX_DEFAULT,
			0,
			D3DFMT_UNKNOWN,
			D3DPOOL_MANAGED,
			D3DX_FILTER_NONE,
			D3DX_FILTER_NONE,
			D3DCOLOR_ARGB(alpha, red, green, blue),
			NULL, NULL,
			&m_pTextures[key])))
		{
			MessageBox(0, "画像の読み込みに失敗しました。", NULL, MB_OK);
		}
	}

	return key;
}

void TextureManager::Release(int key)
{
	m_pTextures[key]->Release();
	m_pTextures[key] = NULL;
}

void TextureManager::ReleaseALL()
{
	for (auto& pTex :m_pTextures)
	{
		if(pTex != NULL)
		{
			pTex->Release();
		}
	}
	std::vector<LPDIRECT3DTEXTURE9>().swap(m_pTextures);
}

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
