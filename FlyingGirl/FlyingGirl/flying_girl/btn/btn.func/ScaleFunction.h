﻿//==================================================================================================================================//
//!< @file		ScaleFunction.h
//!< @brief		ScaleFunctionクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef SCALE_FUNCTION_H
#define	SCALE_FUNCTION_H

#include "ButtonFunction.h"

namespace flying_girl
{
namespace btn
{
namespace func
{

//======================================================================//
//!< ボタンのサイズ変更を行う機能のクラス
//!< @attention BasicButtonクラスと一緒に必ず使用すること
//======================================================================//
class ScaleFunction : public ButtonFunction
{

public:
	/**
	* Constructor
	* @param[in] pbutton	Buttonクラスのインスタンスへのポインタ
	* @param[in] scaleVal	スケール処理の変化量(デフォルト引数 = 10.f)
	*/
	ScaleFunction(Button* pButton, float scaleVal = 10.f);

	/**Destructor*/
	virtual ~ScaleFunction();

private:
	const	float	m_ScaleVal;					//!< スケール処理の変化量
	float			m_CorrectionScaleVal;		//!< 実際にスケール処理に用いる補正値

	/** 処理実行関数 */
	virtual void	Run()override;

	/** 描画処理関数 */
	virtual void	Render()override;

};	// class ScaleFunction

}	// namespace func
}	// namespace btn
}	// namespace flying_girl

#endif	// SCALE_FUNCTION_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
