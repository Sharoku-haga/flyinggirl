﻿//==================================================================================================================================//
//!< @file		PauseMenu.h
//!< @brief		PauseMenuクラスヘッダ
//!< @author	haga
//==================================================================================================================================//

#ifndef PAUSE_MENU_H
#define PAUSE_MENU_H

namespace flying_girl
{
namespace scene
{
namespace game
{
namespace pause
{

class PauseMenu
{

public:
	/** Constructor */
	PauseMenu();

	/** Destructor */
	~PauseMenu();

};	// class PauseMenu

}	// namespace pause
}	// namespace game
}	// namespace scene
}	// namespace flying_girl

#endif	// PAUSE_MENU_H

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
