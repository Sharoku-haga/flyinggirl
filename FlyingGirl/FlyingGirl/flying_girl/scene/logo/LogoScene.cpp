﻿//==================================================================================================================================//
//!< @file		LogoScene.cpp
//!< @brief		LogoSceneクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "LogoScene.h"
#include "LogoBackground.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace scene
{
namespace logo
{

/* Public Functions ------------------------------------------------------------------------------------------- */

LogoScene::LogoScene()
	: m_pBackground(nullptr)
{
	int backgroundTexID = m_rGameLib.LoadTex("../Resource/logo/MyLogo.png");
	m_pBackground		= new LogoBackground(backgroundTexID);
}

LogoScene::~LogoScene()
{
	delete m_pBackground;
	m_pBackground = nullptr;

	m_rGameLib.ReleaseVtxALL();
	m_rGameLib.ReleaseTexALL();
}

/* Private Functions ------------------------------------------------------------------------------------------ */

Scene::ID LogoScene::Control()
{
	if(m_pBackground->Control())
	{	// ロゴがフェードアウトしたらタイトルへ移行
		return Scene::ID::TITLE;
	}
	else
	{
		// テスト用空処理. まだロゴがフェードアウトまで終わってない場合にこの処理に入る
	}

	if(m_rGameLib.CheckKey(DIK_Z) == PUSH)
	{	
		return Scene::ID::TITLE;
	}
	else
	{
		// テスト用空処理. まロゴがフェードアウトまで終わってない、且つキーが押されていない場合にこの処理に入る
	}

	return Scene::ID::LOGO;
}

void LogoScene::Draw()
{
	m_pBackground->Draw();
}

}	// namespace logo
}	// namespace scene
}	// namespace flying_girl

//==================================================================================================================================//
//END OF FILE
//==================================================================================================================================//
