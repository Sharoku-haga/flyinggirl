﻿//==================================================================================================================================//
//!< @file		BrightnessFunction.cpp
//!< @brief		BrightnessFunctionクラス実装
//!< @author	haga
//==================================================================================================================================//

/* Includes --------------------------------------------------------------------------------------------------- */

#include "BrightnessFunction.h"
#include "GameLib/GameLib.h"

/* Namespace -------------------------------------------------------------------------------------------------- */

namespace flying_girl
{
namespace btn
{
namespace func
{

/* Unnamed Namespace ------------------------------------------------------------------------------------------ */

namespace
{

const	DWORD   BrightnessValMax = 0xffffffff;		// 明度最大値

}

/* Public Functions ------------------------------------------------------------------------------------------- */

BrightnessFunction::BrightnessFunction(Button* pButton, DWORD brightnessVal)
	: ButtonFunction(pButton)
	, m_BrightnessVal(brightnessVal)
	, m_CurrntBrightnessVal(brightnessVal)
{}

BrightnessFunction::~BrightnessFunction()
{
	// pButtonのdeleteはButtonFunctionのデストラクタで行う
}

/* Private Functions ------------------------------------------------------------------------------------------ */

void BrightnessFunction::Run()
{
	m_pButton->Control();
	// ボタンを明るくする
	m_CurrntBrightnessVal = BrightnessValMax;
}

void BrightnessFunction::Render()
{
	int	vtxID = m_pButton->GetVtxID();

	m_rGameLib.SetVtxColor(vtxID, m_CurrntBrightnessVal);

	m_pButton->Draw();

	// Vertexを元の状態に戻す
	m_CurrntBrightnessVal = m_BrightnessVal;
	m_rGameLib.SetVtxColor(vtxID, m_CurrntBrightnessVal);
}

}	// namespace func
}	// namespace btn
}	// namespace flying_girl

//==================================================================================================================================//
// END OF FILE
//==================================================================================================================================//
